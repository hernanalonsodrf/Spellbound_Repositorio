using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StarterAssets;

public class RocaRodante : MonoBehaviour
{
    private CharacterController controller;
    private bool isPushing;
    private GameObject player;
    private int counter = 0;
    public int maxCounter = 20;
    public AudioClip clipImpacto;
    private AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isPushing && counter > 0)
        {
            counter--;
            controller.Move((controller.gameObject.transform.position - transform.position).normalized);
            controller.GetComponent<ThirdPersonController>().SetCanJump(false);
            controller.GetComponent<ThirdPersonController>().SetCanMove(false);
            Invoke("ResetMovement", 0.5f);           
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<PlayerCombatManager>() != null)
        {
            audioSource.PlayOneShot(clipImpacto);
            controller = collision.gameObject.GetComponent<CharacterController>();
            controller.gameObject.GetComponent<Animator>().CrossFade("FallAndBackUp", 0.1f);
            controller.gameObject.GetComponent<PlayerDamage>().DealDamage(30, null);
            isPushing = true;
            counter = maxCounter;
            Invoke("DestroyRock", 5f);
        }
    }
    void ResetMovement()
    {
        controller.GetComponent<ThirdPersonController>().SetCanJump(true);
        controller.GetComponent<ThirdPersonController>().SetCanMove(true);
    }
    void DestroyRock()
    {
        GameObject.Destroy(gameObject);
    }
}
