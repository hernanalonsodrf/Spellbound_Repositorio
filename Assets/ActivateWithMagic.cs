using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateWithMagic : MonoBehaviour
{
    [SerializeField] private GameObject objToActivate;
    [SerializeField] private GameObject objToDeactivate;
    [SerializeField] private AudioClip clipAudio;
    [SerializeField] private GameObject particleEffectOnActivation;
    [SerializeField] private GameObject[] particleEffectOnEnabled;

    private AudioSource audioSource;
    private ParticleSystem particlesOnActivation;
    private ParticleSystem otherPortalParticlesOnActivation;
    private List<ParticleSystem> particlesOnEnabled;
    private List<ParticleSystem> otherPortalParticlesOnEnabled;
    private bool itShouldBeScalingNow;
    private int counter = 0;
    [SerializeField] private bool activationEffectScalesUp;    
    [SerializeField] private float timeOffestToScaleUp;

    [Header("Fill up only if this is a portal")]
    [SerializeField] private GameObject otherPortalObjToActivate;
    [SerializeField] private GameObject otherPortalObjToDeactivate;
    [SerializeField] private bool isCityPortal;
    [SerializeField] private GameObject portalToActivate;
    [SerializeField] private GameObject[] otherPortalParticleEffectOnEnabled;



    void Awake()
    {
        itShouldBeScalingNow = false;
        particlesOnEnabled = new List<ParticleSystem>();       
        objToActivate.SetActive(false);
        audioSource = GetComponent<AudioSource>();
        particlesOnActivation = particleEffectOnActivation.GetComponent<ParticleSystem>();
        particleEffectOnActivation.SetActive(false);
        foreach(GameObject o in particleEffectOnEnabled)
        {
            particlesOnEnabled.Add(o.GetComponent<ParticleSystem>());
            o.SetActive(false);
        }
        if (isCityPortal)
        {
            otherPortalObjToActivate.SetActive(false);
            otherPortalParticlesOnEnabled = new List<ParticleSystem>();
            foreach (GameObject o in otherPortalParticleEffectOnEnabled)
            {
                otherPortalParticlesOnEnabled.Add(o.GetComponent<ParticleSystem>());
                o.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Detecta");
        if (other.GetComponent<BulletProjectile>() != null)
        {
            audioSource.PlayOneShot(clipAudio);
            particleEffectOnActivation.SetActive(true);
            foreach (GameObject o in particleEffectOnEnabled)
            {
                o.SetActive(true);
            }
            if (activationEffectScalesUp)
            {
                itShouldBeScalingNow = true;
            }
            if (isCityPortal)
            {                
                foreach (GameObject o in otherPortalParticleEffectOnEnabled)
                {
                    o.SetActive(true);
                }
                foreach (ParticleSystem p in otherPortalParticlesOnEnabled)
                {
                    p.Play();
                }
                otherPortalObjToActivate.SetActive(true);
                otherPortalObjToDeactivate.SetActive(false);
            }
            foreach (ParticleSystem p in particlesOnEnabled)
            {
                p.Play();
            }
            particlesOnActivation.Play();
            objToActivate.SetActive(true);
            objToDeactivate.SetActive(false);
            Invoke("StopParticles", 3.5f);
        }
    }
    private void FixedUpdate()
    {
        if (itShouldBeScalingNow && counter < timeOffestToScaleUp)
        {
            counter++;
            particleEffectOnActivation.transform.localScale += new Vector3(0.05f,0.05f,0.05f);
        }
    }
    private void StopParticles()
    {
        particlesOnActivation.Stop();
        particleEffectOnActivation.SetActive(false);
    }
}
