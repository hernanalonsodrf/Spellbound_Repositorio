using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyDamage : MonoBehaviour
{
    private GameObject player;
    private Stats stats;
    [SerializeField]
    public AudioClip[] sonidosImpactoEspada;
    public AudioClip[] sonidosImpactoProyectil;
    public AudioClip[] sonidosAtaquesAlJugador;
    public AudioClip[] sonidosCombo;
    public AudioClip sonidoStun;
    public AudioClip sonidoBloqueo;
    public AudioSource audioSourceEffects;
    public AudioSource audioSourceVoice;
    public GameObject efectoMuerte;
    public int almasQueSueltaAlMorir;
    private Animator animator;
    public AudioClip muerte;
    public int tiempoReaccion;
    public float tiempoSlowMo;
    public float porcentajeDeVelocidadTiempo;
    public bool seStuneaPorParry;
    private bool puedeMorir;
    private EnemyMeleeAI ia;
    private bool impactaJugador;
    private int contadorUltimoImpacto;
    private int danoAcumulado;//250 son 3s 
    public GameObject efectoVineta;
    private bool debeTransicionar=false;
    private bool puedeSonarAtaque;
    public GameObject panelAlmas;
    public Text textoAlmasGanadas;

    private void OnTriggerEnter(Collider other)
    {        
        if (other.tag == "PlayerWeapon" || (other.GetComponent<BulletProjectile>() != null) && !stats.IsDead())
        {
            if (other.tag == "PlayerWeapon")
            {
                if (ia != null)
                {
                    if (ia.esEscudo && (animator.GetCurrentAnimatorStateInfo(0).IsName("Walk") || animator.GetCurrentAnimatorStateInfo(0).IsName("WalkAlternative")))
                    {
                        animator.CrossFade("Block", 0.1f);
                        audioSourceVoice.PlayOneShot(sonidoBloqueo);
                    }
                    else
                    {
                        audioSourceEffects.clip = GetRandomClipEspada();
                        audioSourceEffects.PlayOneShot(audioSourceEffects.clip);
                        DealDamage(0);
                        SlowDownTime(tiempoSlowMo);
                    }
                }
                else
                {
                    audioSourceEffects.clip = GetRandomClipEspada();
                    audioSourceEffects.PlayOneShot(audioSourceEffects.clip);
                    DealDamage(0);
                    SlowDownTime(tiempoSlowMo);
                }
            }
            else
            {
                if (ia != null)
                {
                    if(Vector3.Distance(transform.position, player.transform.position) > ia.lookRadius)
                    {
                        ia.wasTriggeredByDamageOutOfRange = true;
                    }
                    if (ia.esEscudo && (animator.GetCurrentAnimatorStateInfo(0).IsName("Walk") || animator.GetCurrentAnimatorStateInfo(0).IsName("WalkAlternative")))
                    {
                        animator.CrossFade("Block", 0.1f);
                        audioSourceVoice.PlayOneShot(sonidoBloqueo);
                    }
                    else
                    {
                        audioSourceEffects.clip = GetRandomClipProyectil();
                        audioSourceEffects.PlayOneShot(audioSourceEffects.clip);
                        DealDamage(1);
                    }
                }                
                else
                {
                    audioSourceEffects.clip = GetRandomClipProyectil();
                    audioSourceEffects.PlayOneShot(audioSourceEffects.clip);
                    DealDamage(1);
                }               
            }
        }             
    }

    private void SlowDownTime(float tiempoSlowMo)
    {
        Time.timeScale = porcentajeDeVelocidadTiempo;
        if (!efectoVineta.activeSelf) {
            efectoVineta.SetActive(true);
        }
       
        //transiciones.StartFadeIn();
              
        Invoke("ResetTime", tiempoSlowMo);
    }
    private void ResetTime()
    {
        Time.timeScale = 1;
        //transiciones.StartFadeOut();
        efectoVineta.SetActive(false);
    }

    private void DealDamage(int typeCollision)
    {
        int dmgToDeal = player.GetComponent<Stats>().GetBaseDmg();
        int dmgToIgnore = Mathf.RoundToInt((float)stats.GetDef() * 0.2f);
        int resDmg = dmgToDeal - dmgToIgnore;        
       
        if (typeCollision == 1)//si es bola fuego
        {
            if (ia != null)
            {
                if (ia.enemyType == 1)//si es tanque chupa 80% menos
                {
                    resDmg = (Mathf.RoundToInt((float)resDmg * 0.2f));
                }
                else if (ia.enemyType == 2)//si es d�bil a proyectiles
                {
                    //cupa 30% m�s
                    resDmg = (Mathf.RoundToInt((float)resDmg * 1.3f));
                }
            }            
        }
        else//si es espadazo
        {
            if (ia != null)
            {
                if (ia.enemyType == 1)//si es tanque chupa 20% m�s
                {
                    resDmg = (Mathf.RoundToInt((float)resDmg * 1.2f));
                }
            }               
        }

        if (resDmg <= 0)
        {
            resDmg = 0;
        }
        else
        {
            contadorUltimoImpacto = tiempoReaccion;
            danoAcumulado = danoAcumulado+resDmg;
            if(danoAcumulado>=((stats.GetHp() * 40) / 100)){
                animator.SetTrigger("Stun");
                audioSourceVoice.PlayOneShot(sonidoStun);
                danoAcumulado = 0;
            }
        }
        stats.DealDamage(resDmg);
    }

    // Start is called before the first frame update
    void Start()
    {
        if(efectoMuerte != null)
        {
            efectoMuerte.SetActive(false);
        }        
        stats = GetComponent<Stats>();
        player = GameObject.Find("PlayerArmature");
        animator = GetComponent<Animator>();
        puedeMorir = true;
        ia = GetComponent<EnemyMeleeAI>();
        impactaJugador = false;
        contadorUltimoImpacto = 0;
        danoAcumulado = 0;       
        if (efectoVineta != null)
        {
            efectoVineta.SetActive(false);
        }       
        puedeSonarAtaque = true;     
        
    }
    private void FixedUpdate()
    {
        if (contadorUltimoImpacto > 0)
        {
            contadorUltimoImpacto--;
        }
        else
        {
            danoAcumulado = 0;
        }
        SonidoAtaque();
    }


    // Update is called once per frame
    void Update()
    {
        if (stats.IsDead() && puedeMorir)
        {
            puedeMorir = false;
            audioSourceVoice.PlayOneShot(muerte);
            animator.SetTrigger("Death");
            if (efectoMuerte != null)
            {
                efectoMuerte.SetActive(true);
            }
            Invoke("DestroyEnemy", 1.7f);
        }
    }

    private void DestroyEnemy()
    {
        player.GetComponent<Animator>().GetComponent<EnemyFinder>().DeactivateLockOn();
        player.GetComponent<Stats>().AumentarAlmasJugador(almasQueSueltaAlMorir);
        panelAlmas.SetActive(true);
        textoAlmasGanadas.text ="+" + almasQueSueltaAlMorir.ToString();
        Invoke("DesactivarUIAlmas", 0.75f);           
    }
    private void DesactivarUIAlmas()
    {
        panelAlmas.SetActive(false);
        GameObject.Destroy(gameObject);
    }
    private AudioClip GetRandomClipEspada()
    {
        return sonidosImpactoEspada[UnityEngine.Random.Range(0, sonidosImpactoEspada.Length)];
    }
        private AudioClip GetRandomClipProyectil()
    {
        return sonidosImpactoProyectil[UnityEngine.Random.Range(0, sonidosImpactoProyectil.Length)];
    }

    public void SonidoAtaque()
    {
        if (impactaJugador && puedeSonarAtaque)
        {
            impactaJugador = false;
            puedeSonarAtaque = false;
            AudioClip temp = sonidosAtaquesAlJugador[UnityEngine.Random.Range(0, sonidosAtaquesAlJugador.Length)];
            audioSourceEffects.PlayOneShot(temp);
            Invoke("ResetearSonidoAtaque", 0.5f);
        }        
    }
    public void SetImpactaJugador(bool state)
    {
        impactaJugador = state;
    }

    public void ActivarSonidoCombo()
    {
        AudioClip temp = sonidosCombo[UnityEngine.Random.Range(0, sonidosCombo.Length)];
        audioSourceVoice.PlayOneShot(temp);
    }
    private void ResetearSonidoAtaque()
    {
        puedeSonarAtaque = true;
    }

    public Animator GetAnimator()
    {
        return animator;
    }

    public AudioSource GetAudioSource()
    {
        return audioSourceVoice;
    }
    public AudioClip GetSonidoStun()
    {
        return sonidoStun;
    }
    public bool SeStuneaPorParry()
    {
        return seStuneaPorParry;
    }
}
