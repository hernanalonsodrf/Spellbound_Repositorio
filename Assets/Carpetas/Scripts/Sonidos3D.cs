using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sonidos3D : MonoBehaviour
{
    public bool soloUnaVez;
    public bool loop;
    public int minTimeToLoop;
    public int maxTimeToLoop;
    public AudioClip[] clipsARotar;
    private AudioSource audioSource;
    private bool canLoop;
    private int n;
    // Start is called before the first frame update
    void Start()
    {
        n = 0;
        audioSource = GetComponent<AudioSource>();
        canLoop = true;
    }
   
    private void OnTriggerEnter(Collider other)
    {       
        if (soloUnaVez)
        {
            if (clipsARotar.Length > 0)
            {
                int randomNumber = UnityEngine.Random.Range(0,clipsARotar.Length-1);
                audioSource.clip = clipsARotar[randomNumber];
            }
            audioSource.PlayOneShot(audioSource.clip);
        }
        else if (loop)
        {
            if (clipsARotar.Length > 0)
            {
                int randomNumber = UnityEngine.Random.Range(0, clipsARotar.Length - 1);
                audioSource.clip = clipsARotar[randomNumber];
            }
            int randomNumber2 = UnityEngine.Random.Range(minTimeToLoop, maxTimeToLoop);
            Invoke("ReproducirAudio", randomNumber2);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (loop && canLoop)
        {
            canLoop = false;
            if (clipsARotar.Length > 0)
            {
                int randomNumber = UnityEngine.Random.Range(0, clipsARotar.Length - 1);
                audioSource.clip = clipsARotar[randomNumber];
            }
            int randomNumber2 = UnityEngine.Random.Range(minTimeToLoop, maxTimeToLoop);
            n = randomNumber2;
            Invoke("ReproducirAudio", randomNumber2);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        Invoke("PararAudio", 3f);
    }
    private void PararAudio()
    {
        audioSource.Stop();
    }
    private void ReproducirAudio()
    {
        audioSource.PlayOneShot(audioSource.clip);
        Invoke("RestablecerLoop", n); 
    }
    private void RestablecerLoop()
    {
        canLoop = true;
    }
}
