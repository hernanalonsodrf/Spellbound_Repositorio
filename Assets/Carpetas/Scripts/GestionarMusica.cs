using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestionarMusica : MonoBehaviour
{
    private bool conMusica;
    private bool enCiudad;
    private bool enCombate;
    private AudioSource fuenteAudioCiudad;
    private AudioSource fuenteAudioCombate;

    void Start()
    {
        fuenteAudioCiudad = GameObject.Find("MusicaCiudad").GetComponent<AudioSource>();
        fuenteAudioCombate = GameObject.Find("MusicaCombate").GetComponent<AudioSource>();       
    }
    public bool GetEstadoMusica()
    {
        return conMusica;
    }
    public void SetEnCiudad(bool nuevoEstado)
    {
        enCiudad = nuevoEstado;
        if (nuevoEstado == true)
        {
            fuenteAudioCiudad.Play();
        }
        else
        {
            StartCoroutine(FadeOut(fuenteAudioCiudad, 2f));
        }
    }
    public void SetEnCombate(bool nuevoEstado)
    {
        enCombate = nuevoEstado;
        if (nuevoEstado == true)
        {
            fuenteAudioCombate.Play();
        }
        else
        {
            StartCoroutine(FadeOut(fuenteAudioCombate, 2f));
        }
    }

    public  IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
    {
        float startVolume = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

            yield return null;
        }
        audioSource.Stop();
        audioSource.volume = startVolume;
    }
}
