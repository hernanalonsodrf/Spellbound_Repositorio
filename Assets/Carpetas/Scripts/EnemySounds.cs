using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySounds : MonoBehaviour
{
    private Animator anim;
    private bool isAttacking;
    private bool isIdle;
    private AudioSource audioSource;
    [SerializeField] AudioClip idleSound;
    [SerializeField] AudioClip attackSound;
    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent<Animator>();
        isAttacking = false;
        isIdle = false;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (anim.GetBool("Combat State Active") && !isAttacking)
        {
            isAttacking = true;
            audioSource.clip = attackSound;
            audioSource.Play();
        }
        if(anim.GetBool("Idle Active") && !isIdle)
        {
            isIdle = true;
            audioSource.clip = idleSound;
            audioSource.Play();
        }
        if(!anim.GetBool("Combat State Active"))
        {
            isAttacking = false;
        }
        if (!anim.GetBool("Idle Active"))
        {
            isIdle = false;
        }
    }
}
