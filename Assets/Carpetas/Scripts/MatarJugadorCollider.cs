using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatarJugadorCollider : MonoBehaviour
{
    private GameObject jugador;
    // Start is called before the first frame update
    void Start()
    {
        jugador = GameObject.Find("PlayerArmature");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            jugador.GetComponent<Stats>().DealDamage(jugador.GetComponent<Stats>().GetHp() + 100);
            jugador.GetComponent<Animator>().CrossFade("Death", 0.2f);
            jugador.GetComponent<PlayerDamage>().Invoke("DeathUI", 2f);
        }       
    }
}

