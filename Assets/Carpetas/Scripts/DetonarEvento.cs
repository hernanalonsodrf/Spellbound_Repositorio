using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetonarEvento : MonoBehaviour
{
    public bool detonaUnSonido;
    public bool stuneaAlJugador;
    public bool mueveLaCamara;
    public bool bloqueaElInput;
    public float tiempoQueDuraElEvento;
    public float fuerzaDelShakeDeCamara;
    public bool seDestruye;
    public bool saleTexto;
    public GameObject cuadroTexto;
    public string textoAMostrar;

    public AudioClip sonido;
    public GameObject jugador;

    private StarterAssets.ThirdPersonController personaje;
    private float volumenInicial;
    private CameraShakeCinemachine cameraShake;
    private Animator animator;
    private Text text;

    // Start is called before the first frame update
    void Start()
    {
        personaje = jugador.GetComponent<StarterAssets.ThirdPersonController>();
        volumenInicial = personaje.gameObject.GetComponent<AudioSource>().volume;
        cameraShake = GameObject.Find("PlayerFollowCamera").GetComponent<CameraShakeCinemachine>();
        animator = personaje.gameObject.GetComponent<Animator>();
        if (saleTexto)
        {
            text = cuadroTexto.GetComponentInChildren<Text>();
            cuadroTexto.SetActive(false);
        }       
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (detonaUnSonido)
            {
                personaje.gameObject.GetComponent<AudioSource>().volume = 1f;
                personaje.gameObject.GetComponent<AudioSource>().PlayOneShot(sonido);
                Invoke("ReestablecerVolumen", tiempoQueDuraElEvento);
            }
            if (bloqueaElInput)
            {
                personaje.SetCanJump(false);
                personaje.SetCanMove(false);
                personaje.gameObject.GetComponent<Animator>().SetFloat("Speed", 0f);
                Invoke("DevolverElControl", tiempoQueDuraElEvento);
            }
            if (mueveLaCamara)
            {
                cameraShake.ShakeCamera(fuerzaDelShakeDeCamara, 0.1f);               
            }
            if (stuneaAlJugador)
            {               
                animator.CrossFade("StunSinSonido", 0.25f, 0);               
            }
            if (saleTexto)
            {
                cuadroTexto.SetActive(true);
                text.text = textoAMostrar;
                Invoke("ReestablecerCuadroTexto", tiempoQueDuraElEvento);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (seDestruye)
            {               
                GameObject.Destroy(gameObject);
            }
        }
    }
    private void DevolverElControl()
    {
        personaje.SetCanJump(true);
        personaje.SetCanMove(true);
    }
    private void ReestablecerVolumen()
    {
        personaje.gameObject.GetComponent<AudioSource>().volume = volumenInicial;
    }
    private void ReestablecerCuadroTexto()
    {
        cuadroTexto.SetActive(false);
        text.text = "";
    }
}
