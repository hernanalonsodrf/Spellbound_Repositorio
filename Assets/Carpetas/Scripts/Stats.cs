using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats:MonoBehaviour
{
    private int hp;
    private int def;
    private int baseDmg;
    private int lvl;
    public int currentHp;
    private int almasJugador;
    public HealthBar hpBar;

    [SerializeField] int maxHp;
    [SerializeField] int defense;
    [SerializeField] int baseDamage;
    [SerializeField] int level;

    private bool isPlayer;
    public bool isDead;    
    private bool deadSwitch;
    public Stats(int hp, int def, int baseDmg, int lvl)
    {
        this.hp = hp;
        this.def = def;
        this.baseDmg = baseDmg;
        this.lvl = lvl;
        almasJugador = 0;
    }

    private void Start()
    {
        this.hp = maxHp;
        this.currentHp = maxHp;
        this.def = defense;
        this.baseDmg = baseDamage;
        this.lvl = level;

        deadSwitch = true;

        hpBar.SetMaxHealth(maxHp);

        if (GetComponent<ThirdPersonShooterController>() != null)
        {
            isPlayer = true;
        }
        else
        {
            isPlayer = false;
        }
    }
    
    public void DealDamage(int damage)
    {
        currentHp = currentHp - damage;
        if (currentHp > 0)
        {
            hpBar.SetHealth(currentHp);
        }
        else
        {
            hpBar.SetHealth(0);
            isDead = true;
        }       
    }

    public void LevelUp(int levelToReach)
    {
        float multiplier = 1.2f;
        for(int i=0; i < levelToReach; i++)
        {
            multiplier = multiplier + 0.2f;
        }
        hp = Mathf.RoundToInt((float)(hp* multiplier));
        def = Mathf.RoundToInt((float)(def * multiplier));
        baseDmg = Mathf.RoundToInt((float)(baseDmg * multiplier));
        lvl = Mathf.RoundToInt((float)(lvl * multiplier));
    }

    public bool IsDead()
    {
        if (isDead)
        {            
            return true;
        }
        else
        {
            return false;
        }
    }  
    public HealthBar GetHealthBarScript()
    {
        return hpBar;
    }
    
    public void SetDeathSwitch(bool newState)
    {
        deadSwitch = newState;
    }    

    public int GetCurrentHP()
    {
        return this.currentHp;
    }
    public int GetMaxHP()
    {
        return this.maxHp;
    }
    public void SetCurrentHp(int hp)
    {
        this.currentHp = hp;
        hpBar.SetHealth(currentHp);
    }

    public void SetHp(int hp)
    {
        this.hp = hp;
    }
    public void SetDef(int def)
    {
        this.def = def;
    }
    public void SetBaseDmg(int baseDmg)
    {
        this.baseDmg = baseDmg;
    }
    public void SetLvl(int lvl)
    {
        this.lvl = lvl;
    }
    public int GetHp()
    {
        return this.hp;
    }
    public int GetDef()
    {
        return this.def;
    }
    public int GetBaseDmg()
    {
        return this.baseDmg;
    }
    public int GetLvl()
    {
        return this.lvl;
    }

    public void SetDead(bool newState)
    {
        isDead = newState;
    }
    public int GetAlmasJugador()
    {
        return this.almasJugador;
    }
    public void AumentarAlmasJugador(int n)
    {
        this.almasJugador = GetAlmasJugador() + n;
    }
}
