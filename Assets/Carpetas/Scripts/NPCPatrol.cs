using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCPatrol : MonoBehaviour
{
    public Transform[] puntosDeRuta;
    public int puntoDeRutaActual;
    private NavMeshAgent agente;
    private Animator animator;
    private Vector3 objetivo;
    public bool esCircular;
    private bool yendo;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        agente = GetComponent<NavMeshAgent>();
        puntoDeRutaActual = 0;
        objetivo = Vector3.zero;
        yendo = true;
        ActualizarPuntoDeRuta();        
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, objetivo)<2)
        {
            ActualizarIndiceDeRuta();
            ActualizarPuntoDeRuta();
        }
        
    }   
    void ActualizarPuntoDeRuta()
    {
        objetivo = puntosDeRuta[puntoDeRutaActual].position;
        agente.SetDestination(objetivo);
    }
    void ActualizarIndiceDeRuta()
    {
        if (!esCircular)
        {
            if (yendo)
            {
                puntoDeRutaActual++;
                if (puntoDeRutaActual == puntosDeRuta.Length)
                {
                    puntoDeRutaActual--;
                    yendo = false;
                }
            }
            else
            {
                puntoDeRutaActual--;
                if (puntoDeRutaActual == 0)
                {
                    puntoDeRutaActual++;
                    yendo = true;
                }
            }
        }
        else
        {
            puntoDeRutaActual++;
            if (puntoDeRutaActual == puntosDeRuta.Length)
            {
                puntoDeRutaActual=0;
            }
        }
        
    }
}
