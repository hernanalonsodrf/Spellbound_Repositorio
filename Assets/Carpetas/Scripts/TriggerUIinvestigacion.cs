using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerUIinvestigacion : MonoBehaviour
{
    [SerializeField] private GameObject panel;
    [SerializeField] private GameObject imagenCerca;
    [SerializeField] private GameObject imagenLejos;

    [SerializeField] private bool esCerca;

    void Start()
    {
        panel.SetActive(false);
        imagenCerca.SetActive(false);
        imagenLejos.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Player")
        {            
            panel.SetActive(true);
            if (esCerca)
            {
                imagenCerca.SetActive(true);
                imagenLejos.SetActive(false);
            }
            else
            {
                imagenCerca.SetActive(false);
                imagenLejos.SetActive(true);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!esCerca)
            {
                panel.SetActive(false);
                imagenCerca.SetActive(false);
                imagenLejos.SetActive(false);
            }
            else
            {
                imagenCerca.SetActive(false);
                imagenLejos.SetActive(true);
            }
            
        }
    }

}
