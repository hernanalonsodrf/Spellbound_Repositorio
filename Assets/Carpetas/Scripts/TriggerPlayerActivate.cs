using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerPlayerActivate : MonoBehaviour
{
    [SerializeField] private AudioClip audioClip;
    [SerializeField] private AudioSource audioSource;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            audioSource.clip = audioClip;
            audioSource.Play();
            GameObject.Destroy(gameObject);
        }
    }
}
