using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using StarterAssets;

public class Cinematica : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera cinematicVirtualCamera;
    [SerializeField] private float timeToWaitBeforeCinematic;
    [SerializeField] private float durationTimeCinematic;
    private AudioSource audioSource;
    private ThirdPersonShooterController tpsc;
    private float initialVolume;
    // Start is called before the first frame update
    void Awake()
    {        
        tpsc = FindObjectOfType<ThirdPersonShooterController>();
        audioSource = tpsc.gameObject.GetComponent<AudioSource>();
        initialVolume = audioSource.volume;
        cinematicVirtualCamera.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            audioSource.volume = 0f;
            tpsc.gameObject.GetComponent<ThirdPersonController>().SetCanMove(false);
            tpsc.gameObject.GetComponent<ThirdPersonController>().SetCanJump(false);
            CameraHandler();           
        }
    }

    private void CameraHandler()
    {       
        cinematicVirtualCamera.gameObject.SetActive(true);
        Invoke("Apagar", durationTimeCinematic);
    }
   private void Apagar()
    {
        audioSource.volume = initialVolume;
        cinematicVirtualCamera.gameObject.SetActive(false);
        tpsc.gameObject.GetComponent<ThirdPersonController>().SetCanMove(true);
        tpsc.gameObject.GetComponent<ThirdPersonController>().SetCanJump(true);
        GameObject.Destroy(gameObject);
    }

}
