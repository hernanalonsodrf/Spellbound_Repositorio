using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CruzarTunelLadoALado : MonoBehaviour
{
    public GameObject goblin;
    public GameObject destino;
    public float velocidad;
    private float pasos;
    // Start is called before the first frame update
    void Start()
    {
        goblin.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (goblin.activeSelf == true)
        {            
            pasos = velocidad * Time.deltaTime;
            goblin.transform.position = Vector3.MoveTowards(goblin.transform.position, destino.transform.position, pasos);
        }  
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            goblin.SetActive(true);
            Invoke("DestruirCinematica", 3f);
        }
    }
    void DestruirCinematica()
    {
        goblin.SetActive(false);
        GameObject.Destroy(gameObject);
    }
}
