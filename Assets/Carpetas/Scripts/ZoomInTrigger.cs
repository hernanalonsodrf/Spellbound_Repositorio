using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using StarterAssets;

public class ZoomInTrigger : MonoBehaviour
{
    private ThirdPersonShooterController tpsc;
    public CinemachineVirtualCamera cam;
    public Animator enemyAnimator;
    private GameObject crosshair;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Player")
        {
            enemyAnimator.SetTrigger("Intro");
            cam.gameObject.SetActive(true);
            tpsc.gameObject.GetComponent<ThirdPersonController>().SetCanMove(false);
            tpsc.gameObject.GetComponent<ThirdPersonController>().SetCanJump(false);
            Invoke("ZoomDestroy", 2f);
        }
    }
    private void ZoomDestroy()
    {
        tpsc.gameObject.GetComponent<ThirdPersonController>().SetCanMove(true);
        tpsc.gameObject.GetComponent<ThirdPersonController>().SetCanJump(true);
        cam.gameObject.SetActive(false);
        GameObject.Destroy(gameObject);
    }
    private void Awake()
    {
        cam.gameObject.SetActive(false);
        tpsc = GameObject.Find("PlayerArmature").GetComponent<ThirdPersonShooterController>();
        crosshair = GameObject.Find("Crosshair");        
    }
}
