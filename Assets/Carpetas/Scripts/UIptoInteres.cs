using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIptoInteres : MonoBehaviour
{
    public GameObject objeto;
    public Vector3 tamanhoMinimoObjeto;

    public int contador;
    public int maxContador;
    public bool subiendo;

    // Start is called before the first frame update
    void Start()
    {
        contador = 0;
        subiendo = true;        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (contador == maxContador)
        {
            Reducir();
            subiendo = false;
        }
        if(contador == 0 && !subiendo)
        {
            Amplificar();
            subiendo = true;
        }
        if (contador < maxContador && subiendo)
        {           
            contador++;
        } if(contador > 0 && !subiendo)
        {
            contador--;
        }        
    }
    private void Reducir()
    {
        StartCoroutine(DisminuirEscala());
    }
    private void Amplificar()
    {
        StartCoroutine(AumentarEscala());
    }
    private IEnumerator DisminuirEscala()
    {
        Vector3 tamanho = objeto.transform.localScale;
        for(float n = 0.6f; n>=0; n -= 0.005f)
        {
            tamanho.x = n;
            tamanho.y = n;
            tamanho.z = n;
            objeto.transform.localScale = tamanho;
            yield return null;
        }
       
    }
    private IEnumerator AumentarEscala()
    {
        Vector3 tamanho = objeto.transform.localScale;
        for (float n = 0f; n <= 0.4f; n += 0.005f)
        {
            tamanho.x = n;
            tamanho.y = n;
            tamanho.z = n;
            objeto.transform.localScale = tamanho;
            yield return null;
        }

    }
}
