using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmpujarX : MonoBehaviour
{
    public float velocidad = 1;
    public int maximo;
    public Animator animator;

    private int contador;
    private bool puedeMoverse;
    private int startEndPushAnim;

    private bool isStart;
    private bool isPush;
    private bool isEndPush;

    private void Awake()
    {
        puedeMoverse = true;
        contador = 0;
        startEndPushAnim = Mathf.RoundToInt((float)(maximo * 0.9));
        isStart = true;
        isPush = false;
        isEndPush = false;
    }

    private void Update()
    {
        if (contador>= maximo)
        {
            puedeMoverse = false;
        }
        if (contador == startEndPushAnim)
        {
            animator.SetBool("endPush", true);
            animator.SetBool("isPushing", false);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && puedeMoverse)
        {
            if(isStart)
            {
                isStart = false;
                isPush = true;
                animator.SetBool("isPushing", true);
            }            
            transform.position += transform.forward * Time.deltaTime * velocidad;
            contador++;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        animator.SetBool("isPushing", false);
        animator.SetBool("endPush", true);
        isStart = true;
    }

}
