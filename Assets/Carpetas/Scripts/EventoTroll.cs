using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventoTroll : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioSource musica;
    public AudioClip clipAudio;
    public GameObject troll;

    public void Awake()
    {
        troll.SetActive(false);
        musica.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<BulletProjectile>() != null)
        {
            audioSource.clip = clipAudio;
            audioSource.Play();
            troll.SetActive(true);
            musica.gameObject.SetActive(true);
        }
    }
}
