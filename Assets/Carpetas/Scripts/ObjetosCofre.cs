using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetosCofre : MonoBehaviour
{    
    [SerializeField] public List<int> cantidades;
    [SerializeField] public List<string> nombres;
    public int num;
    private UIManager gUI;
    // Start is called before the first frame update
    void Start()
    {        
        gUI = GameObject.Find("GestorPartida").GetComponent<UIManager>();       
    }   
    public void RellenarUICofre()
    {
        if (nombres.Count > 0)
        {
            gUI.cofreObj1Nombre.text = nombres[0];
        }
        if (nombres.Count > 1)
        {
            gUI.cofreObj2Nombre.text = nombres[1];
        }
        if (nombres.Count > 2)
        {
            gUI.cofreObj3Nombre.text = nombres[2];
        }
        if (cantidades.Count > 0)
        {
            gUI.cofreObj1Cantidad.text = "x " + cantidades[0].ToString();
        }
        if (cantidades.Count > 1)
        {
            gUI.cofreObj2Cantidad.text = "x " + cantidades[1].ToString();
        }
        if (cantidades.Count > 2)
        {
            gUI.cofreObj3Cantidad.text = "x " + cantidades[2].ToString();
        }
    }
}
