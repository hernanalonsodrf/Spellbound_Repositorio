using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidosBlendTree : StateMachineBehaviour
{
    [SerializeField] AudioClip[] golpesSuaves;
    [SerializeField] AudioClip[] golpesFuertes;
    private AudioSource fuenteAudio;
    public string blendTreeParametro;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        fuenteAudio = GameObject.Find("Geometry").GetComponent<AudioSource>();
        AudioClip audio1 = fuenteAudio.clip;
        AudioClip audio2 = fuenteAudio.clip;
        int randomNumber1 = UnityEngine.Random.Range(0, golpesSuaves.Length);
        int randomNumber2 = UnityEngine.Random.Range(0, golpesFuertes.Length);
        for (int i = 0; i < golpesSuaves.Length; i++)
        {
            if (randomNumber1 == i)
            {
                audio1 = golpesSuaves[i];
            }
        }
        for (int i = 0; i < golpesFuertes.Length; i++)
        {
            if (randomNumber2 == i)
            {
                audio2 = golpesFuertes[i];
            }
        }
        if (animator.GetFloat(blendTreeParametro) < 0.3)
        {
            fuenteAudio.clip = audio1;
        }
        else
        {
            fuenteAudio.clip = audio2;
        }        
        fuenteAudio.Play();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
