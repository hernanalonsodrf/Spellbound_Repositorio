using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuntoControl : MonoBehaviour
{
    private bool reached;
    // Start is called before the first frame update
    void Start()
    {
        reached = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            reached = true;
        }
    }
    public Transform GetPosition()
    {
        return transform;
    }
    public bool IsReached()
    {
        return reached;
    }

}
