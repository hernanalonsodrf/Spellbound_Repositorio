using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameSwoosh : StateMachineBehaviour
{
    [SerializeField] AudioClip[] audios;
    private AudioSource fuenteAudio;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (animator.GetBool("isArmed"))
        {
            fuenteAudio = GameObject.Find("sword.OBJ").GetComponent<AudioSource>();           
        }          
        AudioClip audio = fuenteAudio.clip;
        int randomNumber = UnityEngine.Random.Range(0, audios.Length);
        for (int i = 0; i < audios.Length; i++)
        {
            if (randomNumber == i)
            {
                audio = audios[i];
            }
        }

        fuenteAudio.clip = audio;
        fuenteAudio.Play();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
