using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StarterAssets;
using UnityEngine.InputSystem;
using Cinemachine;
using UnityEngine.UI;

public class TPPlayer : MonoBehaviour
{
    [SerializeField] Transform jugador;
    [SerializeField] Transform destino;
    [SerializeField] CinemachineVirtualCamera cam;
    [SerializeField] CinemachineVirtualCamera cam2;
    [SerializeField] bool entrada;
    [SerializeField] bool noSeDeberiaSaltar;
    private AudioSource fuenteAudio;
    public GameObject pantallaNegra;
    private GameObject objetoJugador;
    [SerializeField] AudioClip clipAudio;
    [SerializeField] StarterAssetsInputs starterAssetsInputs;
    private AudioSource ambienteFuenteAudio;
    public AudioClip clipACambiar;
    public GameObject npcsACargar;
    public GameObject npcsADescargar;

    public bool esPortal;
    public bool estaBloqueado;
    private bool textoBloqueoAbierto;
    private bool estaSloweadoPorqEsInterior;

    public GameObject panel;
    public GameObject panelViajeRapido;
    public Text cuadroTexto;
    public AudioClip errorTPSonido;

    private float velocidadMovimiento;
    private float velocidadCorrer;

    // Start is called before the first frame update


    // Update is called once per frame
    void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            //Debug.Log("");
            if (Input.GetKeyDown(KeyCode.E))
            {
                if(esPortal)
                {
                    if (!estaBloqueado)
                    {
                        panel.SetActive(false);
                        //panelViajeRapido.SetActive(true);
                    }
                    else
                    {
                        textoBloqueoAbierto = true;
                        panel.SetActive(true);
                        fuenteAudio.PlayOneShot(errorTPSonido);
                        cuadroTexto.text = "El encantamiento de teletransporte no funciona, necesita energ�a...";
                    }
                }
                else
                {
                    fuenteAudio.PlayOneShot(clipAudio);
                    StartCoroutine(Teleport());
                    if (entrada)
                    {
                        if (cam != null)
                        {
                            cam.gameObject.SetActive(true);
                        }
                        if (noSeDeberiaSaltar)
                        {
                            objetoJugador.GetComponent<ThirdPersonController>().SetCanJump(false);
                            velocidadMovimiento = objetoJugador.GetComponent<ThirdPersonController>().MoveSpeed = objetoJugador.GetComponent<ThirdPersonController>().MoveSpeed;
                            objetoJugador.GetComponent<ThirdPersonController>().MoveSpeed = objetoJugador.GetComponent<ThirdPersonController>().MoveSpeed *0.7f;
                            velocidadCorrer = objetoJugador.GetComponent<ThirdPersonController>().SprintSpeed = objetoJugador.GetComponent<ThirdPersonController>().SprintSpeed;
                            objetoJugador.GetComponent<ThirdPersonController>().SprintSpeed = objetoJugador.GetComponent<ThirdPersonController>().SprintSpeed * 0.8f;
                            estaSloweadoPorqEsInterior = true;
                            objetoJugador.GetComponent<PlayerCombatManager>().isInAnInterior = true;
                        }
                    }
                    else
                    {
                        if (cam != null)
                        {
                            if (cam2 != null)
                            {
                                cam2.gameObject.SetActive(true);
                            }
                            cam.gameObject.SetActive(false);

                        }
                        objetoJugador.GetComponent<ThirdPersonController>().SetCanJump(true);
                        objetoJugador.GetComponent<PlayerCombatManager>().isInAnInterior = false;
                        objetoJugador.GetComponent<ThirdPersonController>().MoveSpeed = velocidadMovimiento;
                        objetoJugador.GetComponent<ThirdPersonController>().SprintSpeed = velocidadCorrer;
                    }
                    if (npcsACargar != null)
                    {
                        npcsACargar.SetActive(true);
                    }
                    else if (npcsADescargar != null)
                    {
                        npcsACargar.SetActive(false);
                    }
                }
            }
            if ((Input.GetKeyDown(KeyCode.Space)|| Input.GetKeyDown(KeyCode.E)
                || Input.GetKeyDown(KeyCode.Escape)) && textoBloqueoAbierto)
            {
                panel.SetActive(false);
            }
        }       
    }
    void Start()
    {
        if (cam != null)
        {
            cam.gameObject.SetActive(false);
        }       
        fuenteAudio = GameObject.Find("PlayerArmature").GetComponent<AudioSource>();
        if (panelViajeRapido != null)
        {
            panelViajeRapido.SetActive(false);
        }       
        pantallaNegra.SetActive(false);
        ambienteFuenteAudio = GameObject.Find("SonidoAmbiente").GetComponent<AudioSource>();
        textoBloqueoAbierto = false;
        objetoJugador= FindObjectOfType<EnemyFinder>().gameObject;
        velocidadMovimiento = objetoJugador.GetComponent<ThirdPersonController>().MoveSpeed;
        velocidadCorrer = objetoJugador.GetComponent<ThirdPersonController>().SprintSpeed;
    }
    public void EmpezarTP()
    {
        fuenteAudio.PlayOneShot(clipAudio);
        StartCoroutine(Teleport());
    }
    IEnumerator Teleport()
    {
        pantallaNegra.SetActive(true);
        yield return new WaitForSeconds(1f);       
        Invoke("ResetearPantalla", 1f);
        jugador.transform.position = destino.transform.position;
    }
    void ResetearPantalla()
    {
        pantallaNegra.gameObject.SetActive(false);
        CambiarMusica();
    }
    void CambiarMusica()
    {
        ambienteFuenteAudio.clip =clipACambiar;
        ambienteFuenteAudio.Play();
    }

}
