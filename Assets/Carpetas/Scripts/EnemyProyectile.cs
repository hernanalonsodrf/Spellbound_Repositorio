using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProyectile : MonoBehaviour
{
    private Rigidbody bulletRigidBody;
    private AudioSource audioSource;
    public AudioClip clipFlying;
    public AudioClip clipImpact;
    public float speed;
    [SerializeField] private Transform vfxExplosion;
    [SerializeField] private List<string> tagToIgnore;

    // Start is called before the first frame update
    void Awake()
    {
        bulletRigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Start()
    {        
        audioSource.clip = clipFlying;
        audioSource.Play();
        bulletRigidBody.velocity = transform.forward * speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (tagToIgnore.Contains(other.tag))
        {

        }
        else
        {
            Instantiate(vfxExplosion, transform.position, Quaternion.identity);
            audioSource.PlayOneShot(clipImpact);
            Invoke("DestroyProyectile",1);
        }
    }
    void DestroyProyectile()
    {
        Destroy(gameObject);
    }
}
