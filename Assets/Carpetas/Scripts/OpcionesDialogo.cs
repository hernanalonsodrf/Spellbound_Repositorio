using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpcionesDialogo : MonoBehaviour
{
    private int contadorRespuesta;
    // Start is called before the first frame update
    void Start()
    {
        contadorRespuesta = 0;
    }
    public int GetContadorRespuesta()
    {
        return contadorRespuesta;
    }
    public void SetContadorRespuesta(int i)
    {
        contadorRespuesta = i;
    } 

    public static void Pregunta1()
    {
        GameObject.Find("PanelOpcionesDialogo").GetComponent<OpcionesDialogo>().SetContadorRespuesta(1);
    }
    public static void Pregunta2()
    {
        GameObject.Find("PanelOpcionesDialogo").GetComponent<OpcionesDialogo>().SetContadorRespuesta(1);
    }
    public static void Pregunta3()
    {
        GameObject.Find("PanelOpcionesDialogo").GetComponent<OpcionesDialogo>().SetContadorRespuesta(1);
    }
    public static void Pregunta4()
    {
        GameObject.Find("PanelOpcionesDialogo").GetComponent<OpcionesDialogo>().SetContadorRespuesta(1);
    }
    public static void Pregunta5()
    {
        GameObject.Find("PanelOpcionesDialogo").GetComponent<OpcionesDialogo>().SetContadorRespuesta(1);
    }
}
