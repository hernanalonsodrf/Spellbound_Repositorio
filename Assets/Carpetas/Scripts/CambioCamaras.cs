using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CambioCamaras : MonoBehaviour
{
    public CinemachineVirtualCamera camAActivar;
    public CinemachineVirtualCamera camADesactivar;
    public GameObject otroCambioCamaras;
    public bool activo;
    private CambioCamaras otroCambio;
    // Start is called before the first frame update
    void Awake()
    {
        if (camAActivar.name != "PlayerFollowCamera")
        {
            camAActivar.gameObject.SetActive(false);
        }        
        otroCambio = otroCambioCamaras.GetComponent<CambioCamaras>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (activo)
        {
            camAActivar.gameObject.SetActive(true);
            camADesactivar.gameObject.SetActive(false);
            otroCambio.SetActivo(true);
        }
        activo = false;       
    }
    public void SetActivo(bool nuevoEstado)
    {
        activo = nuevoEstado;
    }

}
