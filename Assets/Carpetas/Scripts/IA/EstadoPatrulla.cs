using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EstadoPatrulla : MonoBehaviour
{
    public Transform[] WayPoints;

    private MaquinaEstados maquinaEstados;
    private ControladorNavMesh controladorNavMesh;
    private ControladorVision controladorVision;
    private int siguienteWayPoint;
    // Start is called before the first frame update
    void Awake()
    {
        maquinaEstados = GetComponent<MaquinaEstados>();
        controladorNavMesh = GetComponent<ControladorNavMesh>();
        controladorVision = GetComponent<ControladorVision>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (controladorVision.puedeVerAlJugador(out hit))
        {
            controladorNavMesh.perseguirObjetivo= hit.transform;
            maquinaEstados.ActivarEstado(maquinaEstados.EstadoPersecucion);
            return;
        }
        if (controladorNavMesh.HemosLlegado())
        {
            siguienteWayPoint = (siguienteWayPoint+1)%WayPoints.Length;
            ActualizarWayPointDestino();
        }
    }

    private void OnEnable()
    {
        ActualizarWayPointDestino();
    }

    void ActualizarWayPointDestino()
    {
        controladorNavMesh.ActualizarPuntoDestinoNavMeshAgent(WayPoints[siguienteWayPoint].position);

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && enabled)
        {
            maquinaEstados.ActivarEstado(maquinaEstados.EstadoAlerta);
        }
    }
}
