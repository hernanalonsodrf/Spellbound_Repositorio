using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaquinaEstados : MonoBehaviour
{
    public MonoBehaviour EstadoPatrulla;
    public MonoBehaviour EstadoAlerta;
    public MonoBehaviour EstadoPersecucion;
    public MonoBehaviour EstadoInicial;

    private MonoBehaviour EstadoActual;

    // Start is called before the first frame update
    void Start()
    {
        ActivarEstado(EstadoInicial);
    }   

    public void ActivarEstado(MonoBehaviour nuevoEstado)
    {
        if (EstadoActual != null)
        {
            EstadoActual.enabled = false;
        }
        EstadoActual = nuevoEstado;
        EstadoActual.enabled = true;
    }
}
