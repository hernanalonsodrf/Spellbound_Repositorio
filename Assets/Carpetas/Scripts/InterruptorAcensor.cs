using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterruptorAcensor : MonoBehaviour
{
    public GameObject luzVerde;
    public GameObject luzRoja;
    //public GameObject efectoApagado;
    //public GameObject efectoEncendido;
    public GameObject panelDialogo;
    public AudioClip clipAudio;
    private bool switchAbierto;
    private Text texto;

    [Tooltip("Activa un ascensor con efectos.")]
    public Ascensor ascensor;

    public GameObject objetoPuerta;
    private Puerta puerta;

    [Tooltip("Requiere de este �nico interruptor para abrirse.")]
    public bool esUnaPuertaSimple;

    [Tooltip("Requiere de varios interruptores para abrirse.")]
    public bool esUnaPuertaCompleja;
    [Tooltip("El n�mero de interruptor sobre el total para abrirse" +
        "que representa �ste interruptor.")]
    public int numDeInterruptor;

    private void Awake()
    {
        if (objetoPuerta != null)
        {
            puerta = objetoPuerta.GetComponent<Puerta>();
        }        
        if (luzRoja != null)
        {
            luzRoja.SetActive(true);
        }        
        luzVerde.SetActive(false);
        //if (efectoEncendido != null)
        //{
        //    efectoEncendido.SetActive(false);
        //}
       
        switchAbierto = false;
        if (esUnaPuertaCompleja || esUnaPuertaSimple)
        {            
            ascensor.estaOperativo = false;
        }
        if (panelDialogo != null)
        {
            texto = panelDialogo.GetComponentInChildren<Text>();
            panelDialogo.SetActive(false);
        }
    }
    private void Update()
    {
        if (puerta != null)
        {
            if ((esUnaPuertaCompleja || esUnaPuertaSimple) && puerta.GetAbierta() && !switchAbierto)
            {
                switchAbierto = true;
                if (ascensor != null)
                {
                    ascensor.estaOperativo = true;
                }
            }
        }           
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<BulletProjectile>()!=null)
        {
            luzRoja.SetActive(false);
            luzVerde.SetActive(true);
            if (!esUnaPuertaSimple && !esUnaPuertaCompleja)
            {
                //efectoApagado.SetActive(false);
                GetComponent<AudioSource>().clip = clipAudio;
                GetComponent<AudioSource>().Play();                
                //efectoEncendido.SetActive(true);
                if (ascensor != null)
                {
                    ascensor.estaOperativo = true;
                }               
            }
            if (esUnaPuertaSimple)
            {
                GetComponent<AudioSource>().clip = clipAudio;
                GetComponent<AudioSource>().Play();
                puerta.SetAbierta(true);
            }
            if (esUnaPuertaCompleja)
            {
                GetComponent<AudioSource>().clip = clipAudio;
                GetComponent<AudioSource>().Play();
                puerta.GetAbierta();
                puerta.SetInterruptorDelArray(true, numDeInterruptor);
                ActivarInterfaz();
                Invoke("DesActivarInterfaz", 4f);
            }           

        }
    }

    private void ActivarInterfaz()
    {
        panelDialogo.SetActive(true);
        int j = 0;
        for (int i = 0; i < puerta.GetInterruptores().Length; i++)
        {
            if (puerta.GetInterruptores()[i] == true)
            {
                j++;
            }
        }
        if (j == puerta.GetInterruptores().Length)
        {
            texto.text = "�El mecanismo se ha activado por completo!";
        }
        else
        {
            texto.text = "El mecanismo parece estar activ�ndose, pero a�n le falta algo...";
        }        
    }
    private void DesActivarInterfaz()
    {
        panelDialogo.SetActive(false);
        texto.text = null;
    }
}
