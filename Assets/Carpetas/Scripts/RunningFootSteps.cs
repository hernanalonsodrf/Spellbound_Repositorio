using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningFootSteps : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] stoneClips;  
    [SerializeField]
    private AudioClip[] woodClips;
    [SerializeField]   
    private AudioSource audioSource;
    public bool isStone;
    public bool isWood;

    private void Awake()
    {
        //audioSource = GetComponent<AudioSource>();
        audioSource.clip = null;
        isStone = true;
    }

    private void StepRun()
    {
        AudioClip clip = GetRandomClip();
        audioSource.PlayOneShot(clip);
    }

    private AudioClip GetRandomClip()
    {
        if (isStone)
        {
            return stoneClips[UnityEngine.Random.Range(0, stoneClips.Length)];

        }
        else if (isWood)
        {
            return woodClips[UnityEngine.Random.Range(0, woodClips.Length)];
        }
        else
        {
            return stoneClips[UnityEngine.Random.Range(0, stoneClips.Length)];
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Wood")
        {
            isStone = false;
            isWood = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Wood")
        {
            isStone = true;
            isWood = false;
        }
    }
}
