using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StarterAssets;

public class Ascensor : MonoBehaviour
{
    [SerializeField] private GameObject ascensor;
    [SerializeField] private float speed;
    [SerializeField] private int limiteSubida;
    [SerializeField] private AudioClip clipAudio;
    public bool interrruptor;
    private int contador=0;
    public bool estaOperativo = false;
    public bool esDeUnSoloUso;
    public bool hayQueBloquearAlPersonaje;
    public int minAltura;    
    private ThirdPersonController personaje;
    private void Start()
    {
        estaOperativo = false;
        personaje = GameObject.Find("PlayerArmature").GetComponent<ThirdPersonController>();       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && estaOperativo)
        {            
            GetComponent<AudioSource>().clip = clipAudio;
            GetComponent<AudioSource>().Play();
            if (hayQueBloquearAlPersonaje)
            {
                personaje.SetCanJump(false);
                personaje.SetCanMove(false);
                personaje.gameObject.GetComponent<Animator>().SetFloat("Speed", 0f);
                personaje.gameObject.GetComponent<Animator>().CrossFade("Abrir", 0.25f,0);
                Invoke("DevolverElControl", 6f);
            }
        }       
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && estaOperativo)
        {            
            if (!interrruptor)//SUBE
            {
                ascensor.transform.position += ascensor.transform.up * Time.deltaTime * speed;
                contador++;
            }
            else//BAJA
            {
                if (ascensor.transform.position.y > minAltura)
                {
                    ascensor.transform.position -= ascensor.transform.up * Time.deltaTime * speed;
                    contador--;
                }                
            }           
        }       

    }
    private void OnTriggerExit(Collider other)
    {

        if (other.tag == "Player" && estaOperativo)
        {
            GetComponent<AudioSource>().Pause();
            if (contador <= limiteSubida)
            {
                interrruptor = false;//sigue subiendo
            }
            else
            {
                interrruptor = true;//empieza a bajar
            }           
        }
        if (esDeUnSoloUso && estaOperativo)
        {
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
        
    }
    private void DevolverElControl()
    {
        personaje.SetCanJump(true);
        personaje.SetCanMove(true);
    }
    
}
