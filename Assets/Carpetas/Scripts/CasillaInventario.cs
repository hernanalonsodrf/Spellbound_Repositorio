using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CasillaInventario
{
    private bool seleccionada;
    private bool ocupada;
    private Item itemContenido;
    private GameObject objtAlQueHaceReferencia;
    public int tagCasilla;
     
    // Start is called before the first frame update
    public CasillaInventario()
    {
        this.seleccionada = false;
        this.ocupada = false;
        this.itemContenido = null;
        this.tagCasilla = -1;
        this.objtAlQueHaceReferencia = null;
    }

    public void AddItem(Item item)
    {
        if (itemContenido == null)
        {
            itemContenido = item;
            ocupada = true;
        }
        else
        {
            Debug.Log("La casilla ya est� llena.");
        }
    }
    public void VaciarCasilla()
    {
        if (itemContenido != null)
        {
            itemContenido = null;
            ocupada = false;
        }
        else
        {
            Debug.Log("La casilla ya estaba vac�a.");
        }
    }
   public void Seleccionar()
    {
        seleccionada = true;
    }
    public void Deseleccionar()
    {
        seleccionada = false;
    }
    public Item GetItemContenido()
    {
        return itemContenido;
    }
    public bool estaOcupada()
    {
        return ocupada;
    }
    public void SetOcupada(bool cond)
    {
        ocupada = cond;
    }
    public void SetSeleccionada(bool cond)
    {
        seleccionada = cond;
    }
    public void SetNombre(string str)
    {
        itemContenido.SetNombre(str);
    }
    public void SetDescripcion(string str)
    {
        itemContenido.SetDescripcion(str);
    }
    public void SetTag(int tagCasilla)
    {
        this.tagCasilla = tagCasilla;
    }
    public void SetObjtAlQueHaceReferencia(GameObject objtAlQueHaceReferencia)
    {
        this.objtAlQueHaceReferencia = objtAlQueHaceReferencia;
    }    
}
