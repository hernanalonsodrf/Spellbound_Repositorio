using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoEsfuerzo : StateMachineBehaviour
{
    [SerializeField] AudioClip audio1;
    [SerializeField] AudioClip audio2;
    [SerializeField] AudioClip audio3;
    private AudioSource fuenteAudio;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        fuenteAudio = GameObject.Find("PlayerArmature").GetComponent<AudioSource>();
        AudioClip audio = fuenteAudio.clip;
        int randomNumber = UnityEngine.Random.Range(0, 4);
        if (randomNumber == 0)
        {
            audio = audio1;
        }
        if (randomNumber == 1)
        {
            audio = audio2;
        }
        if (randomNumber == 2)
        {
            audio = audio3;
        }
        if (randomNumber == 3)
        {
            audio = null;
        }
        if (randomNumber == 4)
        {
            audio = null;
        }
        
        fuenteAudio.clip = audio;
        fuenteAudio.Play();
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}
           
    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
