using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using StarterAssets;

public class PlayerDamage : MonoBehaviour
{
    private Animator animator;
    private Stats stats;
    private int fallingCounter;
    private Vector3 initialPosition;

    [SerializeField] int maxFallingDmg;
    public GameObject deathUI;
    public GameObject healthBar;
    public GameObject crossHair;
    public GameObject contenedorTextoFrasesMuerte;

    public AudioClip sonidoBloqueo;
    public GameObject colliderBloqueo;    
    [Tooltip("En decenas")] 
    public int porcentajeMaximoBloqueable;
    public string[] frasesMuerte;

    private int lightDmg;
    private int heavyDmg;
   
    private bool freeFallSwitch;
    private AudioSource source;
    private Text frasesMuerteTexto;
    private bool timerParry;

    private StarterAssetsInputs starterAssetsInputs;
    private UIManager ui;

    public bool inTimer;
    public int counterTimer = 20;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        stats = GetComponent<Stats>();
        deathUI.SetActive(false);
        fallingCounter = 0;        
        freeFallSwitch = true;
        starterAssetsInputs = GetComponent<StarterAssetsInputs>();
        lightDmg = Mathf.RoundToInt((float)maxFallingDmg * 0.5f);
        heavyDmg = Mathf.RoundToInt((float)maxFallingDmg * 0.8f);
        source = GameObject.Find("AudioExtra").GetComponent<AudioSource>();
        ui = GameObject.Find("GestorPartida").GetComponent<UIManager>();
        frasesMuerteTexto = contenedorTextoFrasesMuerte.GetComponent<Text>();
        timerParry = true;
        inTimer = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (inTimer)
        {
            if (counterTimer > 0)
            {
                counterTimer--;
            }
            else
            {
                inTimer = false;
            }         
        }
        if (starterAssetsInputs.jump)
        {
            freeFallSwitch = false;
            initialPosition = transform.position;
        }
        else if (animator.GetBool("FreeFall") && freeFallSwitch)
        {
            freeFallSwitch = false;
            initialPosition = transform.position;
        }
        if (animator.GetBool("Grounded"))
        {
            freeFallSwitch = true;
        }
        float fallDistance = Mathf.Abs(initialPosition.y - transform.position.y);
        if (!animator.GetCurrentAnimatorStateInfo(0).IsName("JumpStart") &&
            !animator.GetCurrentAnimatorStateInfo(0).IsName("InAir") &&
            !animator.GetCurrentAnimatorStateInfo(0).IsName("JumpLand") &&
            !animator.GetCurrentAnimatorStateInfo(0).IsName("JumpStartArmed") &&
            !animator.GetCurrentAnimatorStateInfo(0).IsName("InAirArmed") &&
            !animator.GetCurrentAnimatorStateInfo(0).IsName("LandArmed"))
        {
            fallDistance = 0;
        }
        if (fallDistance >= maxFallingDmg && animator.GetBool("Grounded"))
        {   
            DealDamage(stats.GetHp()+1, null);
        }
        if (fallDistance >= heavyDmg && fallingCounter < maxFallingDmg && animator.GetBool("Grounded"))
        {
            DealDamage(Mathf.RoundToInt((float)stats.GetHp()*0.6f), null);
        }
        if (fallDistance >= lightDmg && fallingCounter < heavyDmg && animator.GetBool("Grounded"))
        {
            DealDamage(Mathf.RoundToInt((float)stats.GetHp() * 0.2f), null);
        }               
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "EnemyWeapon")
        {
            if (colliderBloqueo.GetComponent<BoxCollider>().enabled && other.gameObject.GetComponentInParent<EnemyDamage>().SeStuneaPorParry())//Stunear enemigo cuando el jugador le bloquea ataque
            {                
                source.PlayOneShot(sonidoBloqueo);
                other.gameObject.GetComponentInParent<EnemyDamage>().GetAnimator().SetTrigger("Stun");
                other.gameObject.GetComponentInParent<EnemyDamage>().GetAudioSource().PlayOneShot(other.gameObject.GetComponentInParent<EnemyDamage>().GetSonidoStun());
            }
            else
            {
                if (!stats.IsDead() && !inTimer)
                {
                    DealDamage(other.GetComponentInParent<Stats>().GetBaseDmg(), other.gameObject);
                    other.GetComponentInParent<EnemyDamage>().SetImpactaJugador(true);                   
                }                
            }           
        }
        if (other.gameObject.GetComponent<EnemyProyectile>() != null)
        {
            DealDamage(stats.GetBaseDmg()/2, other.gameObject);//se hace a s� mismo su propio da�o
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Piedra")
        {
            if (!stats.IsDead() && !inTimer)
            {
                DealDamage(stats.GetMaxHP() * Mathf.RoundToInt(0.3f), null);               
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "EnemyWeapon")
        {            
            other.GetComponentInParent<EnemyDamage>().SetImpactaJugador(false);
        }
    }

    public void DealDamage(int damage, GameObject other)//'other' es el enemigo que le golpe�
    {        
       
        if (colliderBloqueo.GetComponent<BoxCollider>().enabled)//SI EST� BLOQUEANDO
        {
            if((stats.GetHp() * damage) / 100 < porcentajeMaximoBloqueable){
                damage = 0;
                source.PlayOneShot(sonidoBloqueo);
            }
        }
        if (damage>0)
        {
            stats.DealDamage(damage);
            if (!stats.IsDead())
            {
                StunPlayer(damage);
            }
            else
            {
                if (other != null)
                {
                    if (GetComponent<EnemyFinder>().Scan() != gameObject)
                    {
                        GetComponent<EnemyFinder>().hasToHealAndRepositionEnemies = true;
                        GetComponent<EnemyFinder>().Scan();
                    }                    
                }
                animator.Play("Death", 0, 0);
                Invoke("DeathUI", 2f);
            }
        }       
    }

    private void StunPlayer(int damage)
    {       
            if (((stats.GetHp() * damage) / 100) < 30)//stun leve
            {
                animator.SetFloat("dmgPercentage", 0);
                //Ejecuta la animaci�n
                animator.SetTrigger("Stun");
            }
            else if ((((stats.GetHp() * damage) / 100) > 30) &&
                (((stats.GetHp() * damage) / 100) < 60))//stun medio
            {
                animator.SetFloat("dmgPercentage", 0.4f);
                //Ejecuta la animaci�n
                animator.SetTrigger("Stun");
            }
            else//stun tocho
            {
                animator.SetFloat("dmgPercentage", 1);
                //Ejecuta la animaci�n
                animator.SetTrigger("Stun");
            }
               
    }
    private void DeathUI()
    {
        if (animator.GetBool("IsLockedOn"))
        {
            animator.SetBool("IsLockedOn", false);
        }
        GetComponent<StarterAssets.ThirdPersonController>().SetCanMove(false);
        GetComponent<StarterAssets.ThirdPersonController>().SetCanJump(false);
        deathUI.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        healthBar.SetActive(false);
        crossHair.SetActive(false);
    }
    public void ResetearParry()
    {
        timerParry = true;
    }
}
