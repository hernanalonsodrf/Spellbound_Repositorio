using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using StarterAssets;
using Cinemachine;

public class Dialog : MonoBehaviour
{
    private bool enConversacion;
    public GameObject panel;
    public Text cuadroTexto;
    public GameObject botonInteractuar;   
    public Animator animator;
    public Animator animatorNPC;
    public GameObject healthBar;
    public bool esDialogo;
    public bool esCofre;
    private AudioSource audioSource;
    public AudioClip audioClip;
    private int contador;
    private bool puedeSaltar;
    public CinemachineVirtualCamera cam;
    public string[] lineasConversacion;
    public bool activaOtroScript;
    public GameObject elScriptQueActiva;
    public bool seDestruye;
    private bool seCierraConverPorAlejarse;
    private UIManager gUI;
    private bool yaNoExiste;

    void Awake()
    {
        enConversacion = false;
        contador = 0;
        puedeSaltar = true;
        seCierraConverPorAlejarse = false;
        botonInteractuar.SetActive(false);
        yaNoExiste = false;
        gUI = GameObject.Find("GestorPartida").GetComponent<UIManager>();
        DesactivarUI();
        if (cam != null) {
            cam.gameObject.SetActive(false);
        }
        if (activaOtroScript)
        {
            elScriptQueActiva.GetComponent<Collider>().enabled=false;
        }        
        if (GetComponent<AudioSource>()!=null){
            audioSource = GetComponent<AudioSource>();
        }
    }   
    private void FixedUpdate()
    {
        if (!puedeSaltar)
        {
            animator.GetComponent<StarterAssetsInputs>().jump = false;
        }       
    }

    private void Update()
    {
        if (enConversacion && contador!=0)
        {  
            AvanzarConversacion();    
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !yaNoExiste)
        {
            botonInteractuar.SetActive(true);
        }        
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {  
            if (Input.GetKeyDown(KeyCode.E) && !enConversacion)
            {
                enConversacion = true;
                botonInteractuar.SetActive(false);
                animator.SetBool("Jump", false);
                if (audioClip != null)
                {
                    audioSource.PlayOneShot(audioClip);
                }
                if (esCofre)                {
                   
                    GetComponent<ObjetosCofre>().RellenarUICofre();
                    gUI.tagObjetoCofre = GetComponent<ObjetosCofre>().num;
                    yaNoExiste = true;
                }
                else
                {
                    ActivarUI();
                    LanzarConversacion();
                }               
            }
        }       
    }

    private void OnTriggerExit(Collider other)
    {
        seCierraConverPorAlejarse = true;
        CerrarConversacion();       
        botonInteractuar.SetActive(false);
    }

    private void LanzarConversacion()
    {
        seCierraConverPorAlejarse = false;
        if (animatorNPC != null)
        {
            animatorNPC.SetBool("Talking", true);
        }
        if (cam != null)
        {
            cam.gameObject.SetActive(true);
        }           
        animator.SetBool("IsInteracting", true);
        if (esDialogo)
        {
            cuadroTexto.text = lineasConversacion[0];
        }       
        DesactivarSalto();
        contador++;
    }

    private void AvanzarConversacion()
    {
        DesactivarSalto();
        if (Input.GetKeyDown(KeyCode.Space) && esDialogo)
        {
            if (contador <= lineasConversacion.Length - 1)
            {
                cuadroTexto.text = lineasConversacion[contador];
                contador++;
            }
            else
            {                          
                Invoke("CerrarConversacion", 1);
                               
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CerrarConversacion();            
        }
    }
   
    private void CerrarConversacion()
    {
        DesactivarUI();
        enConversacion = false;        
        //GetComponent<SphereCollider>().enabled=false;
        //GetComponent<SphereCollider>().enabled = true;
        contador = 0;
        puedeSaltar=true;
        animator.SetBool("IsInteracting", false);
        if (cam != null)
        {
            cam.gameObject.SetActive(false);
        }
        if (animatorNPC != null)
        {
            animatorNPC.SetBool("Talking", false);
        }
        contador = 0;
        if (activaOtroScript && !seCierraConverPorAlejarse)
        {
            elScriptQueActiva.GetComponent<Collider>().enabled = true;
        }
        if (seDestruye)
        {
            GameObject.Destroy(gameObject.GetComponent<SphereCollider>());
        }
    }    
    private void ActivarUI()
    {
        panel.SetActive(true);
        healthBar.SetActive(false);
    }

    private void DesactivarUI()
    {
        panel.SetActive(false);
        cuadroTexto.text = null;
        healthBar.SetActive(true);
    }
        
    private void DesactivarSalto()
    {
        puedeSaltar = false;
    }  
}
