using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject barraDeVida;
    public GameObject panelPausa;
    public GameObject cameraTAB;
    public GameObject canvasTABAlt;
    public GameObject cameraTABAlt;
    public GameObject panelCofre;
    public Animator animator;
    public GameObject fraseMenuPausa;
    public GameObject panelTutorial;
    public Text almasJugadorTexto;

    public GameObject panelDescripcionObjetos;
    public Text textoNombreObjeto;
    public Text textoDescripcionObjeto;

    public string[] frasesMenuPausa;
    public AudioClip sonidoUI;
    public AudioClip sonidoAbrirCofre;
    public AudioClip sonidoRecogerObjeto;

    public Text cofreObj1Nombre;
    public Text cofreObj2Nombre;
    public Text cofreObj3Nombre;
    public Text cofreObj1Cantidad;
    public Text cofreObj2Cantidad;
    public Text cofreObj3Cantidad;

    private GameObject jugador;
    private AudioSource fuenteAudio;
    
    private bool enCombate;
    private bool enPausa;
    private bool enTAB;
    private bool pausaPermitida;
    private bool estaSonando;
    private Text textoMenuPausa;
    private List<CasillaInventario> casillas;
    private bool enCofre;
    public int tagObjetoCofre;

    public float tiempoDesaparicionInterfaz;
    private List<Item> objetosInventario;
    public List<string> nombresObjetos;
    public List<string> descripcionesObjetos;
    public List<Image> imagenesObjetos;
    public List<Sprite> spritesObjetos;

    [SerializeField] private Image img0;
    [SerializeField] private Image img1;
    [SerializeField] private Image img2;
    [SerializeField] private Image img3;
    [SerializeField] private Image img4;
    [SerializeField] private Image img5;
    [SerializeField] private Image img6;
    [SerializeField] private Image img7;
    [SerializeField] private Image img8;
    [SerializeField] private Image img9;
    [SerializeField] private Image img10;
    [SerializeField] private Image img11;
    [SerializeField] private Image img12;
    [SerializeField] private Image img13;
    [SerializeField] private Image img14;
    [SerializeField] private Image img15;
    [SerializeField] private Image img16;
    [SerializeField] private Image img17;

    //[SerializeField] private GameObject guanteletes;
    //[SerializeField] private GameObject hombreras;
    //[SerializeField] private GameObject pelaje;
    [SerializeField] private GameObject guardaCuello;
    [SerializeField] private GameObject hombrerasCorrea;
    [SerializeField] private GameObject hombrerasMetal;
    [SerializeField] private GameObject guanteleteIzq;
    [SerializeField] private GameObject guanteleteDr;

    // Start is called before the first frame update
    void Start()
    {
        enCombate = false;
        barraDeVida.SetActive(false);
        panelPausa.SetActive(false);
        canvasTABAlt.SetActive(false);
        enPausa = false;
        pausaPermitida = true;
        jugador = GameObject.Find("PlayerArmature");
        fuenteAudio = GameObject.Find("GestorPartida").GetComponent<AudioSource>();
        enTAB = false;
        textoMenuPausa = fraseMenuPausa.gameObject.GetComponent<Text>();
        objetosInventario = new List<Item>();
        casillas = new List<CasillaInventario>();
        imagenesObjetos = new List<Image>();
        spritesObjetos = new List<Sprite>();
        panelCofre.SetActive(false);
        enCofre = false;
        tagObjetoCofre = -1;
        guardaCuello.SetActive(false);
        guanteleteIzq.SetActive(false);
        guanteleteDr.SetActive(false);
        hombrerasCorrea.SetActive(false);
        hombrerasMetal.SetActive(false);
        //pelaje.SetActive(false);

        for (int i = 0; i < nombresObjetos.Count; i++)
        {
            objetosInventario.Add(new Item(nombresObjetos[i], descripcionesObjetos[i], 1, null, false));
        }
               
        for(int i=0; i < 18; i++)
        {
            if (objetosInventario.Count > i)
            {
                CasillaInventario casilla = new CasillaInventario();
                casilla.AddItem(objetosInventario[i]);
                casillas.Add(casilla);
            }
            else
            {
                CasillaInventario casilla = new CasillaInventario();                
                casillas.Add(casilla);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {       
        if (animator.GetBool("isArmed"))
        {
            enCombate = true;
        }
        else
        {
            enCombate = false;
        }
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("StunnedArmed") ||
            animator.GetCurrentAnimatorStateInfo(0).IsName("StunnedUnarmed"))
        {
            barraDeVida.SetActive(true);                   
        }
        else
        {
            if (!enCombate)
            {
                Invoke("DesActivarInterfaz", tiempoDesaparicionInterfaz);              
            }
        }        
    }  
    
   


}
