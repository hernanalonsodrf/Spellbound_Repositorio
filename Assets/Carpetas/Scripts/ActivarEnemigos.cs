using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivarEnemigos : MonoBehaviour
{
    public GameObject[] enemigos;
    private AudioSource fuenteAudio;
    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject enemigo in enemigos)
        {
            enemigo.SetActive(false);
        }
        fuenteAudio = GetComponent<AudioSource>();
    }
    private void OnTriggerEnter(Collider other)
    {        
        if (other.tag == "Player")
        {
            if (fuenteAudio != null)
            {
                fuenteAudio.Play();
            }            
            foreach (GameObject enemigo in enemigos)
            {
                enemigo.SetActive(true);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        GameObject.Destroy(gameObject);
    }
}
