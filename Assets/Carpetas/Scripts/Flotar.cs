using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StarterAssets;

public class Flotar : MonoBehaviour
{
    public bool estaSubiendo;
    public float velocidad;    
    private GameObject jugador;
    // Start is called before the first frame update
    void Start()
    {
        estaSubiendo = false;
        jugador = GameObject.Find("Player");       
    }    
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            jugador.transform.parent = transform;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            jugador.transform.parent = null;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {        
        if (estaSubiendo)
        {
            transform.position += transform.forward * Time.deltaTime * velocidad;            
        }
        else
        {
            transform.position -= transform.forward * Time.deltaTime * velocidad;           
        }
    }   

}
