using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StarterAssets;

public class HacerCombo : StateMachineBehaviour
{
    private bool repiteAtaque;
    public int frameACambiar;
    public int frameABloquear;
    public string siguienteEstado;
    public int numCombo;
    private float tiempo;
    public bool esFinalDelCombo;
    public float velocidadDeTransicion;
    public int tiempoAEsperar;
    
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        repiteAtaque = false;
        tiempo=0;
        animator.SetInteger("numCombo", numCombo);
        animator.gameObject.GetComponent<ThirdPersonController>().MoveSpeed = animator.gameObject.GetComponent<ThirdPersonController>().MoveSpeed * 0.4f;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //animator.gameObject.GetComponent<ThirdPersonController>().SetCanMove(false);
        if(animator.GetCurrentAnimatorStateInfo(0).IsName("Attack") || animator.GetCurrentAnimatorStateInfo(0).IsName("Attack2") || animator.GetCurrentAnimatorStateInfo(0).IsName("Attack3"))
        {
            animator.gameObject.GetComponent<ThirdPersonController>().SetCanMove(true);
            animator.gameObject.GetComponent<ThirdPersonController>().MoveSpeed = animator.gameObject.GetComponent<ThirdPersonController>().MoveSpeed * 0.4f;
        }
        if (!esFinalDelCombo)
        {
            tiempo++;           
            
            if (Input.GetMouseButtonDown(0) && tiempo > frameACambiar && tiempo < frameABloquear && !repiteAtaque)
            {                
                repiteAtaque = true;
                animator.CrossFade(siguienteEstado, velocidadDeTransicion, 0, 0f);
            }
        }        
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        repiteAtaque = false;
        animator.SetInteger("numCombo", 0);
        animator.gameObject.GetComponent<ThirdPersonController>().MoveSpeed = animator.gameObject.GetComponent<ThirdPersonController>().originalMoveSpeed;
    }

    IEnumerator ResetSpeed(Animator anim, int layerIndex)
    {
        yield return new WaitForSeconds(tiempoAEsperar);
        anim.gameObject.GetComponent<ThirdPersonController>().MoveSpeed = anim.gameObject.GetComponent<ThirdPersonController>().originalMoveSpeed;
    }
   
    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
