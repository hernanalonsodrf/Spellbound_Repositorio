using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialUI : MonoBehaviour
{
    public GameObject mensaje;
    public string textoMensaje;
    public GameObject botonCerrar;
    public bool esDeRalentizarTiempo;
    public bool esDeRecorrido;
    public bool esDeCerrarConBotonUI;
    public bool esDeCerrarConTecla;
    public bool esDeCerrarConMouseButton;
    public bool esDeCerrarConRuedaRaton;
    public bool seDestruye;
    public float cantidadReduccionTiempo;
    public int keyCodeTeclaAPulsar;
    public int mouseButtonAPulsar;

    private KeyCode tecla;
    private Text text;
    // Start is called before the first frame update
    void Start()
    {
        mensaje.SetActive(false);
        if (esDeCerrarConTecla)
        {
            AsignarTecla();
        }
        botonCerrar.SetActive(false);
        text = mensaje.GetComponentInChildren<Text>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            mensaje.SetActive(true);//activar mensaje si es q no se cierra con teclas, reducir tiempo mientras se est� dentro
            text.text = textoMensaje; 
            if (esDeRecorrido)
            {                 
                Time.timeScale = cantidadReduccionTiempo;                
            }
            else
            {
                if (esDeRalentizarTiempo)
                {
                    Time.timeScale = cantidadReduccionTiempo;
                }                
                if (esDeCerrarConBotonUI)
                {
                    Cursor.visible = true;
                    Cursor.lockState = CursorLockMode.None;
                }               
                if (!esDeCerrarConBotonUI)
                {
                    botonCerrar.SetActive(false);
                }
                else
                {
                    botonCerrar.SetActive(true);
                }
            }            
        }        
    }
    //A�ADIR ON CLICK
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {           
            if (esDeCerrarConTecla)
            {
                if (Input.GetKeyDown(tecla))
                {
                    CerrarMensaje();
                }
            }if (esDeCerrarConMouseButton)
            {
                if (Input.GetMouseButton(mouseButtonAPulsar))
                {
                    CerrarMensaje();
                }
            }if(esDeCerrarConRuedaRaton)
            {
                if (Input.GetAxis("Mouse ScrollWheel") > 0)
                {
                    CerrarMensaje();
                }
            } 
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (esDeRecorrido)
            {
                CerrarMensaje();
            }
            if (seDestruye)
            {
                Destroy(gameObject);
            }
            Invoke("CerrarMensaje", 1f);
        }
    }
    private void ResetearTiempo()
    {
        Time.timeScale = 1f;
    }
    public void CerrarMensaje()
    {
        mensaje.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        if (esDeCerrarConBotonUI)
        {
            botonCerrar.SetActive(false);
        }
        ResetearTiempo();
    }
    public void AsignarTecla()
    {
        switch (keyCodeTeclaAPulsar)
        {
            case 0:
                {
                    tecla =KeyCode.E;
                } break;
            case 1:
                {
                    tecla = KeyCode.R;
                }
                break;
            case 2:
                {
                    tecla = KeyCode.Space;
                }
                break;
            case 3:
                {
                    tecla = KeyCode.F;
                }
                break;
            case 4:
                {
                    tecla = KeyCode.Tab;
                }
                break;
            case 5:
                {
                    tecla = KeyCode.Escape;
                }
                break;
            default:
                {

                }break;
        }
    }   
    
}
