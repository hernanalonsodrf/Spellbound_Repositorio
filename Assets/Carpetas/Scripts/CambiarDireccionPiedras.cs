using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiarDireccionPiedras : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "CambioDireccion")
        {
            GetComponentInParent<Flotar>().estaSubiendo = !GetComponentInParent<Flotar>().estaSubiendo;
        }
    }
}
