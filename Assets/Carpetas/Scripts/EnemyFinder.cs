using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class EnemyFinder : MonoBehaviour
{
    //This script searches for enemies nearby and manages the "lock-on camera"
    //feature, it also heals and repositions groups of enemies who kill the player.

    public float range;
    private CinemachineVirtualCamera cam;
    private CinemachineVirtualCamera camLockOn;
    private bool isAlreadyOnLockOn;
    private GameObject target;
    private CharacterController controller;
    private Animator animator;
    private GameObject crosshair;
    private GestionarMusica gestorMusica;
    private bool switchMusica;
    public bool hasToHealAndRepositionEnemies;
    public List<GameObject> enemigosParaResetearPosicion;
    private Vector3 originalRotation;   
   
    void Awake()
    {
        InitializeVariables();
    }
    private void Update()
    {
        //Activate music when in combat, deactivate when not
        ManageCombatMusic();
        //When should lock on be activated and when deactivated
        ManageLockOnInputAndActivation();
        //What to do when player is locking on an enemy
        ManageLockOnBehaviour();
    }
    private void ManageLockOnBehaviour()
    {
        //make player rotation face the locked on enemy 
        if (animator.GetBool("IsLockedOn"))
        {
            Vector3 aimDirection = Vector3.zero;
            aimDirection = (transform.position - Scan().transform.position).normalized;
            transform.forward = Vector3.Lerp(transform.forward, -aimDirection, Time.deltaTime * 20f);
        }
        float horizontalSpeed = controller.velocity.x;
        //player is moving right, animate it as strifing right
        if (horizontalSpeed > 0)
        {
            animator.SetBool("MovingRight", true);
            animator.SetBool("MovingLeft", false);
        }//player is moving left, animate it as strifing left
        else if (horizontalSpeed < 0)
        {
            animator.SetBool("MovingRight", false);
            animator.SetBool("MovingLeft", true);
        }
        else//player is moving backwards or forward
        {
            animator.SetBool("MovingRight", false);
            animator.SetBool("MovingLeft", false);
        }
    }
    private void ManageLockOnInputAndActivation()
    {
        //if not already in lock on, then activate lock
        if (Input.GetKeyDown(KeyCode.F) && !isAlreadyOnLockOn)
        {
            isAlreadyOnLockOn = true;
            LockOn();
        }//if is already locking or if tries to lock on but is far away, then can't lock 
        else if (Input.GetKeyDown(KeyCode.F) && isAlreadyOnLockOn ||
         Vector3.Distance(cam.transform.position, cam.LookAt.position) > range)
        {
            isAlreadyOnLockOn = false;            
            DeactivateLockOn();
        }//if weapon isn't out, can't try to lock
        if (!animator.GetBool("isArmed"))
        {           
            DeactivateLockOn();
        }
    }
    public void LockOn()
    {
        if (animator.GetBool("isArmed"))
        {//if returned gameObject is not the actual player, lock on to it
            if (Scan() != gameObject)
            {
                animator.SetBool("IsLockedOn", true);
                cam.enabled = false;
                camLockOn.enabled = true;
                target = Scan();
                camLockOn.LookAt = target.transform;
                target.gameObject.GetComponent<LockOnUI>().lockOnUI.SetActive(true);
            }
        }       
    }
    public GameObject Scan()
    {
        float distance = float.PositiveInfinity;
        Collider resCol = gameObject.GetComponent<Collider>();

        Collider[] closeEnemies = Physics.OverlapSphere(transform.position, range);

        foreach (Collider other in closeEnemies)
        {
            if (other.tag == "AI")
            {
                float temp = Vector3.Distance(other.transform.position, cam.transform.position);
                if (temp < distance)//Always locks on closest enemy
                {
                    resCol = other;
                }
                HealAndRepositionEnemiesWhenPlayerIsKilled(other);
            }
        }
        hasToHealAndRepositionEnemies = false;
        return resCol.gameObject;
    }
    public void DeactivateLockOn()
    {
        if (target != null)
        {
            target.gameObject.GetComponent<LockOnUI>().lockOnUI.SetActive(false);
        }
        animator.SetBool("IsLockedOn", false);
        cam.enabled = true;
        cam.transform.eulerAngles = originalRotation;
        camLockOn.enabled = false;
    }
    private void HealAndRepositionEnemiesWhenPlayerIsKilled(Collider other)
    {
        if (hasToHealAndRepositionEnemies)
        {
            other.GetComponent<Stats>().SetCurrentHp(other.GetComponent<Stats>().GetHp());
            other.GetComponent<Animator>().Play("Idle");
            if (other.GetComponent<EnemyMeleeAI>() != null)
            {
                other.GetComponent<EnemyMeleeAI>().DesactivarColliderArma();
            }            
            DeactivateLockOn();
            enemigosParaResetearPosicion.Add(other.gameObject);
        }
    }
    public GameObject GetTarget()
    {
        return target;
    }
    private void ManageCombatMusic()
    {
        if (Scan() != gameObject)
        {
            if (!switchMusica)
            {
                if (Vector3.Distance(Scan().transform.position, transform.position) < range / 2)
                {
                    switchMusica = true;
                    Invoke("ActivarMusicaCombate", 1.5f);
                }
                else
                {
                    if (switchMusica)
                    {
                        switchMusica = false;
                        gestorMusica.SetEnCombate(false);
                    }
                }
            }
        }
    }
    private void ActivateCombatMusic()
    {
        gestorMusica.SetEnCombate(true);
    }
    private void InitializeVariables()
    {
        controller = GetComponent<CharacterController>();
        cam = GameObject.Find("PlayerFollowCamera").GetComponent<CinemachineVirtualCamera>();
        camLockOn = GameObject.Find("PlayerLockOnCamera").GetComponent<CinemachineVirtualCamera>();
        camLockOn.enabled = false;
        isAlreadyOnLockOn = false;
        animator = gameObject.GetComponent<Animator>();
        target = null;
        crosshair = GameObject.Find("Crosshair");
        //gestorMusica = GameObject.Find("GestorPartida").GetComponent<GestionarMusica>();
        switchMusica = false;
        hasToHealAndRepositionEnemies = false;
        originalRotation = cam.transform.localEulerAngles;
    }
}
