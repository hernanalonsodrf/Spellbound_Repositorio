using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambioDeMusica : MonoBehaviour
{
    private GestionarMusica gestorMusica;
    private UIManager gestorUI;
    // Start is called before the first frame update
    void Start()
    {
        gestorUI = GameObject.Find("GestorPartida").GetComponent<UIManager>();
        gestorMusica = GameObject.Find("GestorPartida").GetComponent<GestionarMusica>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" )
        {
            gestorMusica.SetEnCiudad(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            gestorMusica.SetEnCiudad(false);
                      
        }
    }
}
