using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puerta : MonoBehaviour
{
    private bool abierta;
    public bool[] interruptores;
    public GameObject panelTexto;
    public string texto;
    // Start is called before the first frame update
    void Start()
    {
        abierta = false;
        for (int i = 0; i < interruptores.Length; i++)
        {
            interruptores[i] = false;
        }
        panelTexto.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Player" && !abierta)
        {
            panelTexto.SetActive(true);
            panelTexto.GetComponentInChildren<Text>().text = texto;
        }
        
    }
    private void OnTriggerExit(Collider other)
    {
        panelTexto.SetActive(false);       
    }
    // Update is called once per frame
    void Update()
    {
        int j = 0;
        for (int i = 0; i < interruptores.Length; i++)
        {
            if (interruptores[i] == true)
            {
                j++;
            }           
        }
        if (j == interruptores.Length)
        {
            SetAbierta(true);
        }
    }
    public bool GetAbierta()
    {
        return abierta;
    }
    public void SetAbierta(bool estado)
    {
        abierta = estado;
    }
    public void SetInterruptorDelArray(bool estado, int posicion)
    {
        interruptores[posicion] = estado;
    }
    public bool[] GetInterruptores()
    {
        return interruptores;
    }
}
