using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargaEscenario : MonoBehaviour
{
    public GameObject[] escenariosACargar;
    public GameObject[] escenariosADescargar;
    public GameObject otroTrigger;
    private bool activo;
    public bool esEntrada;
    // Start is called before the first frame update
    void Start()
    {
        if (esEntrada)
        {
            activo = false;
        }
        else
        {
            activo = true;
        }        
    }   
    private void OnTriggerEnter(Collider other)
    {
        if (activo)
        {
            if (other.tag == "Player")
            {
                activo = false;  
                foreach(GameObject objeto in escenariosACargar)
                {
                    objeto.SetActive(true);
                }
                foreach (GameObject objeto in escenariosADescargar)
                {
                    objeto.SetActive(false);
                }              
            }
        }       
    }
    private void OnTriggerExit(Collider other)
    {
        otroTrigger.GetComponent<CargaEscenario>().SetActivo(true);
    }
    public void SetActivo(bool estado)
    {
        activo = estado;
    }
}
