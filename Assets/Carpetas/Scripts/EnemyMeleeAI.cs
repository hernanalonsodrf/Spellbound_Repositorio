using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class EnemyMeleeAI : MonoBehaviour
{
	public float lookRadius;
	public float meleeRange;
	public float maxAreaRange;	
	public Animator playerAnimator;

	public Transform player;
	NavMeshAgent agent;
	Animator animator;
	Stats stats;
	PlayerDamage playerDmg;

	public bool patruya;
	public bool esADistancia;
	public Transform posicionOriginal;
	public float maxDistOrigen;
	public bool usaAtaquesAleatorios;

	public Transform proyectil;
	public Transform posicionLanzamiento;
	public Transform piesJugador;

	public GameObject colliderArma;
	public GameObject colliderArma2;		

	private bool canAttackAgain;
	private bool canShootAgain;
	public bool esArquero;
	public bool esEscudo;
	private bool inScene;
	private bool isUnawareOfPlayer;

	public float ataqueCD;
	public float lanzamientoCD;
	[Tooltip("mel�e b�sico:0, boss mel�e:1, d�bil a fuego:2")]
	public int enemyType;

	public Vector3 initialPos;
	private bool inArea;
	private GameObject puntoRetorno;
	private float originalLookRadius;
	public GameObject lockOnUI;

	private bool isAttacking;
	private bool isFollowingPlayer;
	private bool isInPatrol;
	private bool isInMeleeRange;
	private float distanceToPlayer;
	private float distToOrigin;
	private bool hasToDash = false;
	public bool wasTriggeredByDamageOutOfRange = false;
	private bool canDash = true;
	public float dashingSpeed;
	public GameObject colliderImpactoSalto;
	public AudioClip audioClipJumpImpact;
	public GameObject particlesJumpImpact;

	void Start()
	{
		//target = GameObject.Find("PlayerArmature").transform;
		playerDmg = player.GetComponent<PlayerDamage>();
		agent = GetComponent<NavMeshAgent>();
		animator = GetComponent<Animator>();
		stats = GetComponent<Stats>();
		animator.SetBool("IsMoving", false);
		DesactivarColliderArma();
		canAttackAgain = true;
		inScene = true;
		isUnawareOfPlayer = true;
        if (esADistancia)
        {
			canShootAgain = true;
		}
        if (colliderImpactoSalto != null)
        {
			colliderImpactoSalto.SetActive(false);
        }
        if (particlesJumpImpact != null)
        {
			particlesJumpImpact.GetComponent<ParticleSystem>().Stop();
        }
		initialPos = transform.position;
		inArea = true;
		puntoRetorno = new GameObject();
		puntoRetorno.transform.position = initialPos;
		originalLookRadius = lookRadius;
		lockOnUI = GetComponentInChildren<FindCanvasLockOn>().gameObject;
		lockOnUI.SetActive(false);

		isAttacking = false;
		isFollowingPlayer = false;
		isInMeleeRange = false;
		distanceToPlayer = 0f;
	}    

    void Update()
	{
        if (!stats.IsDead())
        {
			UpdateAnimationStates();
			CheckDistances();
			MainBehaviour();
        }
	}
	private void UpdateAnimationStates()
    {
        if(distToOrigin < 5 && distanceToPlayer > lookRadius && !wasTriggeredByDamageOutOfRange)
        {
			agent.isStopped = true;
			animator.SetBool("IsMoving", false);
		}       
		if (animator.GetCurrentAnimatorStateInfo(0).IsName("Walk"))
        {
			isFollowingPlayer = true;
        }
        else
        {
			isFollowingPlayer = false;
        }
		if (animator.GetCurrentAnimatorStateInfo(0).IsName("Attack") || animator.GetCurrentAnimatorStateInfo(0).IsName("Attack 2"))
		{
			isAttacking = true;
        }
        else
        {
			isAttacking = false;
        }
	}
	private void CheckDistances()
    {
		distanceToPlayer = Vector3.Distance(player.position, transform.position);
		distToOrigin = Vector3.Distance(initialPos, transform.position);		
	}
	private void MainBehaviour()
    {
        if (!player.GetComponent<Stats>().IsDead())
        {
			if ((!isAttacking && distanceToPlayer < lookRadius) && distToOrigin < maxAreaRange)
			{
				FaceTarget(player);
			}
			if (distanceToPlayer < lookRadius && distToOrigin < maxAreaRange) //Si el jugador est� en rango de visi�n
			{//y dentro de la zona de actuaci�n de la IA,
				wasTriggeredByDamageOutOfRange = false;
				if (distanceToPlayer > meleeRange)//pero a�n no est� en rango de melee,
				{
                    if (enemyType == 1)//Si es un troll
                    {
						if (canShootAgain)//y el lanzamiento de piedras no est� en CD
						{
							canShootAgain = false;							
							ShootAtPlayer();
                        }
                        else
                        {
							if(distanceToPlayer > 6 && canDash)
                            {								
								Dash();
                            }
							FollowPlayer();//persigue al jugador.
						}
                    }
                    else
                    {
						FollowPlayer();//persigue al jugador.
					}					
				}
				else//Si est� en rango de melee
				{
					if (!isAttacking && canAttackAgain)// y no est� atacando ya y no est� en CD,
					{
						AttackPlayer();//ataca al jugador.
					}
				}
			}
			else
			{
                if (wasTriggeredByDamageOutOfRange)
                {
					FollowPlayer();
                }
				if (!inArea && distToOrigin > maxAreaRange)//Si la IA est� fuera del area de actuaci�n
				{
					if (distanceToPlayer <= lookRadius)//y el jugador sigue a rango de visi�n,
					{
						ReduceViewDistance();//reduce el rango de visi�n para que lo deje de targetear.
					}
					GoBackToInitialPosition();//Vuelve a la posici�n original.
				}
				if(distToOrigin > 5 && distanceToPlayer > lookRadius && !wasTriggeredByDamageOutOfRange)//Si el jugador est� fuera del rango de visi�n, vuelve.
				{
					GoBackToInitialPosition();
				}
			}
		}		
	}
	void GoBackToInitialPosition()
    {
		FaceTarget(puntoRetorno.transform);
		agent.isStopped = false;
		GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
		animator.SetBool("IsMoving", true);
		agent.SetDestination(initialPos);
	}
	void FollowPlayer()
    {
		if (usaAtaquesAleatorios)
		{
			animator.SetInteger("numAttack", 0);
			animator.SetInteger("numWalk", UnityEngine.Random.Range(1, 3));
		}
		agent.isStopped = false;
		GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
		animator.SetBool("IsMoving", true);
		isUnawareOfPlayer = false;
		agent.SetDestination(player.position);				
	}
	void AttackPlayer()
    {
        if (!player.GetComponent<Stats>().IsDead())
        {
			agent.isStopped = true;
			animator.SetBool("IsMoving", false);
			GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

            if (!usaAtaquesAleatorios)
            {
				if (animator.GetInteger("combo") == 0)
				{
					animator.SetInteger("combo", 1);
				}
				else
				{
					animator.SetInteger("combo", 0);
				}
			}
			
			// Attack			
            if (usaAtaquesAleatorios)
            {
				animator.SetInteger("numAttack", UnityEngine.Random.Range(1,5));
            }
            else
            {
				animator.SetTrigger("Attack");
			}
			canAttackAgain = false;
			Invoke("ResetearAtaque", ataqueCD);
		}		
	}
	private void Dash()
    {
		canDash = false;
		agent.isStopped = false;
		agent.SetDestination(player.position);
		agent.speed = agent.speed * 30;
		agent.acceleration = agent.acceleration * 2;
		animator.SetTrigger("Dash");		
		Invoke("ResetSpeed", 1f);
	}

	private void ReduceViewDistance()
    {
		lookRadius = 3;
		Invoke("RecuperarDistanciaVisionado", 2f);
    }
	private void RecuperarDistanciaVisionado()
	{
		lookRadius = originalLookRadius;
	}
	void Huir()
    {
		animator.SetBool("IsMoving", true);
		isUnawareOfPlayer = false;
		Vector3 direccionAlJugador = transform.position - player.transform.position;
		Vector3 nuevaPos = transform.position + direccionAlJugador;
		agent.SetDestination(nuevaPos);
		Debug.Log("huyendo");
	}
	void ShootAtPlayer()
	{
		animator.SetTrigger("Shoot");
		Invoke("InstantiateProyectile", 0.8f);
		Invoke("ResetShootingCD", lanzamientoCD);
	}

	// Point towards the player
	void FaceTarget(Transform target)
	{        
		Vector3 direction = (target.position - transform.position).normalized;
		Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
		transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);				
	}	
	
	private List<GameObject> Scan()
	{
		Collider resCol = gameObject.GetComponent<Collider>();
		//Vector3 resDir = Vector3.zero;
		Collider[] colliders = Physics.OverlapSphere(transform.position, 15f);
		List<GameObject> enemigos = new List<GameObject>();
		foreach (Collider other in colliders)
		{
			if (other.tag == "AI")
			{
				enemigos.Add(other.gameObject);
			}
		}
		return enemigos;
	}

	private void ResetearAtaque()
    {
		canAttackAgain = true;
	}
	private void ResetShootingCD()
	{
		canShootAgain = true;
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, lookRadius);
	}

	private void ActivarColliderArma()
	{
        if (colliderArma != null)
        {
			colliderArma.GetComponent<BoxCollider>().enabled = true;

		}
		if (colliderArma2 != null)
		{
			colliderArma2.GetComponent<BoxCollider>().enabled = true;

		}
	}
	public void DesactivarColliderArma()
	{
		if (colliderArma != null)
		{
			colliderArma.GetComponent<BoxCollider>().enabled = false;

		}
		if (colliderArma2 != null)
		{
			colliderArma2.GetComponent<BoxCollider>().enabled = false;

		}
	}
	private void ActivarColliderImpactoSalto()
	{
		if (colliderImpactoSalto != null)
		{
			particlesJumpImpact.GetComponent<ParticleSystem>().Play();
			GetComponent<AudioSource>().PlayOneShot(audioClipJumpImpact);
			colliderImpactoSalto.SetActive(true);
		}		
	}
	public void DesactivarColliderImpactoSalto()
	{
		if (colliderImpactoSalto != null)
		{
			colliderImpactoSalto.SetActive(false);
		}		
	}
	private void ResetSpeed()
    {
		agent.speed = agent.speed / 30;
		agent.acceleration = agent.acceleration / 2;
		Invoke("ResetDash", 4f);
    }
	private void ResetDash()
    {
		canDash = true;
    }
	private void InstantiateProyectile()
	{
		Vector3 direction = new Vector3();
		direction = Vector3.zero;
		if (enemyType == 1)
        {
			direction = new Vector3(player.transform.position.x - 1, player.transform.position.y - 1.5f, player.transform.position.z);
		}
        else
        {
			direction = player.transform.position;
        }
		Vector3 _shootDirection = (transform.position - direction).normalized;
		Instantiate(proyectil, posicionLanzamiento.position, Quaternion.LookRotation(-_shootDirection, Vector3.up));
	}
}
