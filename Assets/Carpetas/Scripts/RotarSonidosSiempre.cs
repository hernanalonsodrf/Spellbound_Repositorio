using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotarSonidosSiempre : StateMachineBehaviour
{
    [SerializeField] AudioClip[] audios;
    public bool aVecesNoSuena;
    [Header("1-5")]
    public int probabilidadNoSonar;

    private AudioSource fuenteAudio;
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        fuenteAudio = GameObject.Find("Geometry").GetComponent<AudioSource>();
        AudioClip audio = fuenteAudio.clip;
        int randomNumber = UnityEngine.Random.Range(0, audios.Length);
        for(int i = 0; i < audios.Length; i++)
        {
            if (randomNumber == i)
            {
                audio = audios[i];
            }
        }
        if (aVecesNoSuena)
        {
            int randomNumber2 = UnityEngine.Random.Range(0, 5);
            if (randomNumber2 < probabilidadNoSonar)
            {
                audio = null;
            }
        }

        fuenteAudio.clip = audio;
        fuenteAudio.Play();
    }
    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
