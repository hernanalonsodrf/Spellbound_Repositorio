using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectile : MonoBehaviour
{
    private Rigidbody bulletRigidBody;
    [SerializeField] private Transform vfxExplosion;
    [SerializeField] private List<string> tagToIgnore;
    public bool DebugImpact;

    // Start is called before the first frame update
    void Awake()
    {
        bulletRigidBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Start()
    {
        float speed = 18f;
        bulletRigidBody.velocity = transform.forward * speed;        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (tagToIgnore.Contains(other.tag))
        {

        }
        else
        {
            Instantiate(vfxExplosion, transform.position, Quaternion.identity);
            //Debug.Log(other.name);
            Destroy(gameObject);
        }
        if (DebugImpact)
        {
            Debug.Log(other.gameObject.name);
        }
    }
}
