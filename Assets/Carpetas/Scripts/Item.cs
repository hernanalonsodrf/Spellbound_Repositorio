using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item 
{
    private string nombre;
    private string descripcion;
    private int cantidad;
    private Image imagen;
    private bool esStackeable;

    public Item(string nombre, string descripcion, int cantidad, Image imagen, bool cond)
    {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.esStackeable = cond;
        this.cantidad = 0;
        this.imagen = imagen;
    }
    public Item()
    {
        this.nombre = "";
        this.descripcion = "";
        this.cantidad = 0;
        this.imagen = null;
        esStackeable = true;
    }
    public void AumentarCantidad()
    {
        if (esStackeable)
        {
            cantidad++;
        }
        else
        {
            Debug.Log("No es stackeable.");
        }
    }
    public string GetNombre()
    {
        return this.nombre;
    }
    public string GetDescripcion()
    {
        return this.descripcion;
    }
    public void SetNombre(string str)
    {
        nombre = str;
    }
    public void SetDescripcion(string str)
    {
        descripcion = str;
    }
}
