using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMageAI : MonoBehaviour
{
    public float rangoAtaque;
    public GameObject jugador;
    public Transform posicionLanzamiento;
    public Transform proyectil;
    public float lanzamientoCD;
    private bool puedeLanzar;
    private Animator animator;
    public Vector3 posInicial;
    // Start is called before the first frame update
    void Start()
    {
        posInicial = transform.position;
        puedeLanzar = true;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float distancia = Vector3.Distance(transform.position, jugador.transform.position);
        if (distancia <= rangoAtaque)
        {
            MirarAlObjetivo(jugador.transform);
            if (puedeLanzar)
            {
                puedeLanzar = false;
                animator.SetTrigger("Shoot");
                DispararJugador();
            }           
        }
    }
    void DispararJugador()
    { 
        Invoke("InvocarDisparo", 0.8f);
        Invoke("ResetearLanzamiento", lanzamientoCD);
    }
    void MirarAlObjetivo(Transform objetivo)
    {
        Vector3 direction = (objetivo.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
    }
    private void ResetearLanzamiento()
    {
        puedeLanzar = true;
    }
    private void InvocarDisparo()
    {
        Vector3 _shootDirection = (transform.position - jugador.transform.position).normalized;
        Instantiate(proyectil, posicionLanzamiento.position, Quaternion.LookRotation(-_shootDirection, Vector3.up));
    }
}
