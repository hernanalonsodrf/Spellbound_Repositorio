using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reaparicion : MonoBehaviour
{
    public Animator animator;
    public Stats stats;
    public GameObject deathUI;
    public PlayerDamage pdmg;
    public GameObject healthBar;
    public GameObject crossHair;
    private HealthBar hpBar;
    public PuntoControl[] puntosControl;
    private Transform player;
    private EnemyFinder ef;
    private List<GameObject> enemigosAEliminar;
   
    // Start is called before the first frame update
    void Start()
    {       
        hpBar = animator.gameObject.GetComponent<Stats>().GetHealthBarScript();
        player = GameObject.Find("PlayerArmature").transform;
        ef = player.GetComponent<EnemyFinder>();
        enemigosAEliminar = new List<GameObject>();
    }
    //Devuelve el punto de reaparici�n m�s cercano que ya haya sido
    //alcanzado por el jugador.
    private Vector3 BuscarPuntoDeReaparicion()
    {
        Vector3 pt=Vector3.zero;
        float min = float.MaxValue;
        foreach (PuntoControl ptoControl in puntosControl)
        {
            float temp= Vector3.Distance(ptoControl.GetPosition().position, player.position);
            if (temp < min && ptoControl.IsReached())
            {
                min = temp;
                pt = ptoControl.transform.position;
            }
        }      
        return pt;
    }
    
    public void Reaparecer()
    {
        if (BuscarPuntoDeReaparicion() != Vector3.zero)
        {
            player.transform.position = BuscarPuntoDeReaparicion();
        }
        player.GetComponent<StarterAssets.ThirdPersonController>().SetCanMove(true);
        player.GetComponent<StarterAssets.ThirdPersonController>().SetCanJump(true);
        animator.Play("Idle Walk Run Blend");
        stats.SetCurrentHp(stats.GetHp());
        stats.SetDead(false);
        stats.SetDeathSwitch(true);      
        deathUI.SetActive(false);
        healthBar.SetActive(true);
        crossHair.SetActive(true);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        stats.SetCurrentHp(stats.GetHp());
        hpBar.SetHealth(stats.GetHp());
        foreach (GameObject obj in ef.enemigosParaResetearPosicion)
        {
            if (obj.GetComponent<EnemyMeleeAI>() != null)
            {
                obj.transform.position = obj.GetComponent<EnemyMeleeAI>().initialPos;
            }else if (obj.GetComponent<EnemyMageAI>() != null)
            {
                obj.transform.position = obj.GetComponent<EnemyMageAI>().posInicial;
            }
            enemigosAEliminar.Add(obj);            
        }
        foreach (GameObject obj in enemigosAEliminar)
        {
            if (ef.enemigosParaResetearPosicion.Contains(obj))
            {
                ef.enemigosParaResetearPosicion.Remove(obj);
            }
        }
        player.GetComponent<PlayerDamage>().inTimer = true;
    }
}
