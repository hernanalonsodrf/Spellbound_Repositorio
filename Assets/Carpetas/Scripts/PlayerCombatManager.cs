using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StarterAssets;

public class PlayerCombatManager : MonoBehaviour
{
    private Animator animator;
    [SerializeField] float resetAtackTimer;
    [SerializeField] float resetBlockTimer;
    [SerializeField] float resetKnockUpTimer;
    [SerializeField] float resetDodgeTimer;
    private bool canAttack;
    private bool puedePatada;
    private bool puedeBloqueo;
    private bool puedeEsquivar;
    private bool canDragWeaponOut;
    private bool switchApuntarEfecto;
    private bool bloquearSwitch;
    private bool conArma;

    public GameObject arma;
    public GameObject efectoActivarArma;
    public GameObject efectoSlash;
    public GameObject efectoSlash1;
    public GameObject efectoSlash2;
    public GameObject colliderArma;
    public GameObject colliderEscudo;
    public int tiempoCombo;
    public bool isInAnInterior;

    private ParticleSystem particulas;
    private ParticleSystem slash;
    private ParticleSystem slash1;
    private ParticleSystem slash2;

    private EnemyFinder enmFndr;

    private int comboIteracion;
    private bool puedeCombo;
    public int timerCombo;
    public int clicks;
    private bool interruptor1;
    private bool interruptor2;
    private bool hasToLookAtEnemy;
    private GameObject enemyToLookAt;

    // Start is called before the first frame update
    void Start()
    {
        InitializeVariables();
    }

    // Update is called once per frame
    void Update()
    {
        //Manages input and particles to drag the weapon out and back in
        SheatheAndUnsheatheWeapon();
        //Makes weapon invisible when casting magic and visible when slashing or in idle
        ManageWeaponVisibility();
        //Creates slash particles when swinging the sword around
        ManageComboParticles();

        Attack();
        Block();
        Run();
        if (hasToLookAtEnemy)
        {
            Invoke("ResetLookAtEnemy", 1f);
            Vector3 aimDirection = Vector3.zero;
            if (enemyToLookAt != null)
            {
                aimDirection = (transform.position - enemyToLookAt.transform.position).normalized;
                transform.forward = Vector3.Lerp(transform.forward, -aimDirection, Time.deltaTime * 20f);
            }            
            
        }
    }
    private void SheatheAndUnsheatheWeapon()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0 && animator.GetBool("Grounded") &&
            canDragWeaponOut && !conArma && !isInAnInterior)
        {
            conArma = true;
            GetComponent<ThirdPersonController>().MoveSpeed = GetComponent<ThirdPersonController>().MoveSpeed - 1.5f;
            GetComponent<ThirdPersonController>().SprintSpeed = GetComponent<ThirdPersonController>().SprintSpeed - 1.5f;
            canDragWeaponOut = false;
            ActivarArma1();
            Invoke("ResetearCambioArma", 1.5f);
        } else if (Input.GetAxis("Mouse ScrollWheel") < 0 && animator.GetBool("Grounded") &&
             canDragWeaponOut)
        {
            conArma = false;
            GetComponent<ThirdPersonController>().MoveSpeed = GetComponent<ThirdPersonController>().MoveSpeed + 1.5f;
            GetComponent<ThirdPersonController>().SprintSpeed = GetComponent<ThirdPersonController>().SprintSpeed + 1.5f;
            canDragWeaponOut = false;
            DesactivarArma();
            Invoke("ResetearCambioArma", 1.5f);
        }
    }
    private void ManageComboParticles()
    {
        if (animator.GetInteger("numCombo") == 1 && interruptor1)
        {
            StopSlash();
            interruptor1 = false;
            interruptor2 = true;
            Invoke("ActivarSlashCombo1", 0.4f);
        }
        if (animator.GetInteger("numCombo") == 2 && interruptor2)
        {
            interruptor2 = false;
            interruptor1 = true;
            Invoke("ActivarSlashCombo2", 0.5f);
        }
        if (animator.GetInteger("numCombo") == 0)
        {
            interruptor1 = true;
            interruptor2 = true;
        }
    }

    private void ManageWeaponVisibility()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("KnockUp")
            || animator.GetBool("isAiming")
            || animator.GetCurrentAnimatorStateInfo(0).IsName("Casting")
            || animator.GetCurrentAnimatorStateInfo(0).IsName("CastLockedOn"))
        {
            arma.SetActive(false);
        }

        else
        {
            if (animator.GetBool("isArmed"))
            {
                if (arma.activeSelf == false)
                {
                    Invoke("ActivarArma1", 0.1f);
                }
            }
        }
    }

    private void Attack()
    {
        if (animator.GetBool("isArmed") && !animator.GetBool("IsLockedOn"))
        {
            if (Input.GetMouseButtonDown(0) &&
           (animator.GetCurrentAnimatorStateInfo(0).IsName("Armed Blend Tree") ||
           animator.GetCurrentAnimatorStateInfo(0).IsName("Attack") ||
           animator.GetCurrentAnimatorStateInfo(0).IsName("Attack2")) &&
           !animator.GetCurrentAnimatorStateInfo(0).IsName("Casting")
           && !animator.GetBool("isAiming"))
            {
                if (canAttack && !animator.GetBool("IsDoingCombo") && animator.GetInteger("numCombo") == 0)
                {
                    canAttack = false;
                    animator.SetTrigger("Attack");
                    Invoke("ActivarSlash", 0.2f);
                    Invoke("ResetearAtaque", resetAtackTimer);
                }
            }
        }
        else if (animator.GetBool("IsLockedOn"))
        {
            if (Input.GetMouseButtonDown(0) &&
           (animator.GetCurrentAnimatorStateInfo(0).IsName("StrafeRight") ||
           animator.GetCurrentAnimatorStateInfo(0).IsName("StrafeLeft") ||
           animator.GetCurrentAnimatorStateInfo(0).IsName("Idle")) &&
           canAttack && !animator.GetCurrentAnimatorStateInfo(0).IsName("Casting")
           && !animator.GetBool("isAiming"))
            {
                if (canAttack && !animator.GetBool("IsDoingCombo") && animator.GetInteger("numCombo") == 0)
                {
                    canAttack = false;
                    animator.SetTrigger("Attack");
                    Invoke("ActivarSlash", 0.2f);
                    Invoke("ResetearAtaque", resetAtackTimer);
                }
            }
        }

    }
    private void Block()
    {
        if (animator.GetBool("isArmed"))
        {
            if (Input.GetKeyDown(KeyCode.R) &&
            ((animator.GetCurrentAnimatorStateInfo(0).IsName("Armed Blend Tree") ||
            animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))) && puedeBloqueo)
            {
                canAttack = false;
                puedeBloqueo = false;
                if (enmFndr != null)
                {
                    GameObject enemy = enmFndr.Scan();
                    if (enemy != gameObject)//Looks to enemy when blocking
                    {
                        hasToLookAtEnemy = true;
                        enemyToLookAt = enemy;
                    }
                }               
                animator.SetTrigger("Block");
                Invoke("ResetearAtaque", resetBlockTimer);
                Invoke("ResetearBloqueo", resetBlockTimer);
            }
        }
        if (animator.GetBool("isArmed2"))
        {
            if (Input.GetMouseButtonDown(1) &&
           ((animator.GetCurrentAnimatorStateInfo(0).IsName("Armed2Idle") ||
           animator.GetCurrentAnimatorStateInfo(0).IsName("Armed2Walk") ||
           animator.GetCurrentAnimatorStateInfo(0).IsName("Armed2Run") ||
           animator.GetCurrentAnimatorStateInfo(0).IsName("Attack 0"))) &&
           bloquearSwitch)
            {
                bloquearSwitch = false;
                canAttack = false;
                animator.SetBool("isBlock2", true);
            }
            if (Input.GetMouseButtonUp(1) && (animator.GetCurrentAnimatorStateInfo(0).IsName("Block 0")
                || animator.GetCurrentAnimatorStateInfo(0).IsName("BlockStart")))
            {
                animator.SetBool("isBlock2", false);
                canAttack = true;
                bloquearSwitch = true;
            }
        }
        if (animator.GetBool("IsLockedOn"))
        {
            if (Input.GetKeyDown(KeyCode.R) &&
           (animator.GetCurrentAnimatorStateInfo(0).IsName("StrafeRight") ||
           animator.GetCurrentAnimatorStateInfo(0).IsName("StrafeLeft") ||
           animator.GetCurrentAnimatorStateInfo(0).IsName("Idle")) &&
           canAttack && !animator.GetCurrentAnimatorStateInfo(0).IsName("Casting")
           && !animator.GetBool("isAiming"))
            {
                canAttack = false;
                puedeBloqueo = false;
                animator.SetTrigger("Block");
                Invoke("ResetearAtaque", resetBlockTimer);
                Invoke("ResetearBloqueo", resetBlockTimer);
            }
        }
    }   
    private void Run()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            animator.SetBool("isShift", true);
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            animator.SetBool("isShift", false);
        }
    }
    private void InitializeVariables()
    {
        ResetearAtaque();
        animator = GetComponent<Animator>();
        arma.SetActive(false);
        animator.SetBool("isArmed", false);
        canAttack = true;
        puedePatada = true;
        puedeBloqueo = true;
        puedeEsquivar = false;
        canDragWeaponOut = true;
        efectoActivarArma.SetActive(true);
        switchApuntarEfecto = true;
        bloquearSwitch = true;
        hasToLookAtEnemy = false;
        conArma = false;
        enemyToLookAt = new GameObject();
        particulas = efectoActivarArma.GetComponent<ParticleSystem>();
        slash = efectoSlash.GetComponent<ParticleSystem>();
        slash1 = efectoSlash1.GetComponent<ParticleSystem>();
        slash2 = efectoSlash2.GetComponent<ParticleSystem>();
        StopParticulas();
        StopSlash();
        DesactivarColliderArma();
        DesactivarColliderEscudo();
        comboIteracion = 0;
        puedeCombo = false;
        timerCombo = 0;
        clicks = 0;
        interruptor1 = true;
        interruptor2 = true;
        enmFndr = GetComponent<EnemyFinder>();
    }
    private void ActivarColliderArma()
    {
        colliderArma.GetComponent<BoxCollider>().enabled = true;
    }
    private void DesactivarColliderArma()
    {
        colliderArma.GetComponent<BoxCollider>().enabled = false;
    }
    private void ActivarColliderEscudo()
    {
        colliderEscudo.GetComponent<BoxCollider>().enabled = true;
    }
    private void DesactivarColliderEscudo()
    {
        colliderEscudo.GetComponent<BoxCollider>().enabled = false;
    }
    private void ResetearCambioArma()
    {
        canDragWeaponOut = true;
    }
    private void ResetLookAtEnemy()
    {
        hasToLookAtEnemy=false;
        enemyToLookAt = null;
    }
    private void ResetearAtaque()
    {
        canAttack = true;        
    }
    private void ResetearEsquive()
    {
        puedeEsquivar = true;
    }
    private void ResetearPatada()
    {
        puedePatada = true;
    }
    private void ResetearBloqueo()
    {
        puedeBloqueo = true;
    }
    private void StopParticulas()
    {
        particulas.Stop();
    }
    private void StopSlash()
    {
        slash.Stop();
        slash1.Stop();
        slash2.Stop();
    }
    private void ActivarSlash()
    {
        slash.Play();
        Invoke("StopSlash", 0.5f);
    }
    private void ActivarSlashCombo1()
    {
        slash1.Play();
        Invoke("StopSlash", 0.5f);
    }
    private void ActivarSlashCombo2()
    {
        slash2.Play();
        Invoke("StopSlash", 0.5f);
    }
    private void DesactivarArma()
    {
        animator.SetBool("isArmed", false);
        particulas.Play();
        Invoke("StopParticulas", 0.5f);
        arma.SetActive(false);
    }   
    private void ActivarArma1()
    {       
        animator.SetBool("isArmed", true);
        arma.SetActive(true);
        particulas.Play();
        Invoke("StopParticulas", 0.5f);        
    }      
}
