using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonAI : MonoBehaviour
{
    [SerializeField] private GameObject dragonHead;
    [SerializeField] private GameObject player;
    [SerializeField] private int viewRange;
    [SerializeField] private float lanzamientoCD;
    private bool puedeLanzar;
    public Transform posicionLanzamiento;
    public Transform proyectil;
    public Vector3 direction;
    // Start is called before the first frame update
    void Start()
    {
        dragonHead.SetActive(false);
        puedeLanzar = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(transform.position, player.transform.position) <= viewRange)
        {
            dragonHead.SetActive(true);
            if (puedeLanzar)
            {
                puedeLanzar = false;
                DispararJugador();
            }           
        }
        else
        {
            dragonHead.SetActive(false);  
        }
    }
    void DispararJugador()
    {
        Invoke("InvocarDisparo", 0.8f);
        Invoke("ResetearLanzamiento", lanzamientoCD);
    }  
    private void ResetearLanzamiento()
    {
        puedeLanzar = true;
    }
    private void InvocarDisparo()
    {
        Vector3 _shootDirection = (transform.position - player.transform.position).normalized;
        Instantiate(proyectil, posicionLanzamiento.position, Quaternion.LookRotation(-_shootDirection, direction));
    }
}
