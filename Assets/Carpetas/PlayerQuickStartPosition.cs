using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerQuickStartPosition : MonoBehaviour
{
    //Este script deja al jugador en el lugar deseado al darle a PLAY

    [Header("Marcar solo una casilla")]
    [SerializeField] private bool vaAUsarseElScript;
    [SerializeField] private bool hoguera;
    [SerializeField] private bool troll;
    [SerializeField] private bool piedrasFlotantesArriba;
    [SerializeField] private bool piedrasFlotantesAbajo;
    [SerializeField] private bool puente;
    [SerializeField] private bool ciudad;
    [SerializeField] private bool emboscada;
    [SerializeField] private bool antesala;
    [SerializeField] private bool dragon;


    // Start is called before the first frame update
    void Start()
    {
        if (vaAUsarseElScript)
        {
            Vector3 pos = new Vector3();
            pos = Vector3.zero;
            if (hoguera)
            {
                pos = new Vector3(13.15f, 17.4f, 206.35f);
                transform.parent.position = pos;
            }
            else if (troll)
            {
                pos = new Vector3(39.31f, 17.4f, 123.63f);
                transform.parent.position = pos;
            }else if (piedrasFlotantesArriba)
            {
                pos = new Vector3(-153.4f, 85.58f, 196.43f);
                transform.parent.position = pos;
            }else if (piedrasFlotantesAbajo)
            {
                pos = new Vector3(-521.31f, 28.25f, 224.27f);
               transform.parent.position = pos;
            }
            else if (puente)
            {
                pos = new Vector3(160.14f, 49.167f, -198.56f);
                transform.parent.position = pos;
            }
            else if (ciudad)
            {
                pos = new Vector3(-829.9f, 9.47f, 46f);
                transform.parent.position = pos;
            }
            else if (emboscada)
            {
                pos = new Vector3(394.72f, 39.93f, -328.87f);
                transform.parent.position = pos;
            }
            else if (antesala)
            {
                pos = new Vector3(599.441f, 48.11f, -441.99f);
                transform.parent.position = pos;
            }
            else if (dragon)
            {
                pos = new Vector3(745.84f, 66.01f, -522.01f);
                transform.parent.position = pos;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
