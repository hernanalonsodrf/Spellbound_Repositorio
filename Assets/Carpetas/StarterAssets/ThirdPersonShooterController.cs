using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using StarterAssets;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;

public class ThirdPersonShooterController : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera aimVirtualCamera;
    [SerializeField] private float normalSensitivity;
    [SerializeField] private float aimSensitivity;
    [SerializeField] private LayerMask aimColliderLayerMask = new LayerMask();
    [SerializeField] private Transform pfBulletProjectile;
    [SerializeField] private Transform spawnBulletPosition;
    [SerializeField] private Transform spawnBulletPositionLockedOn;
    [SerializeField] private GameObject chargeSphere;
    [SerializeField] private GameObject chargePointLight;

    private ThirdPersonController thirdPersonController;
    private StarterAssetsInputs starterAssetsInputs;
    private Animator anim;
    private Vector3 aimDir;
    private GameObject lockOnCamera;
    private EnemyFinder enmyFndr;
    private bool lockAimSwitch;
    private bool canShoot;

    public GameObject crosshair;

    private void Awake()
    {
        thirdPersonController = GetComponent<ThirdPersonController>();
        starterAssetsInputs = GetComponent<StarterAssetsInputs>();
        anim = GetComponent<Animator>();
        chargeSphere.SetActive(false);
        chargePointLight.SetActive(false);
        aimDir = Vector3.zero;
        lockOnCamera= GameObject.Find("PlayerLockOnCamera");
        enmyFndr = GetComponent<EnemyFinder>();
        lockAimSwitch = true;
        canShoot = true;
        crosshair.SetActive(false);
    }      

    private void Update()
    {     
        Vector3 mouseWorldPosition = Vector3.zero;
        Vector2 screenCenterPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
        Transform hitTransform = null;       
        Ray ray = Camera.main.ScreenPointToRay(screenCenterPoint);


        if (anim.GetBool("IsLockedOn") && anim.GetBool("isArmed"))
        {
            thirdPersonController.SetCanRotate(false);
        }
        else
        {
            thirdPersonController.SetCanRotate(true);
        }      
        if (Physics.Raycast(ray, out RaycastHit raycastHit, 999f, aimColliderLayerMask))
        {
            mouseWorldPosition = raycastHit.point;
            hitTransform = raycastHit.transform;

        }

        if (starterAssetsInputs.aim && !anim.GetBool("isArmed2") && !anim.GetBool("IsLockedOn"))
        {
            crosshair.SetActive(true);
            chargeSphere.SetActive(true);
            chargePointLight.SetActive(false);
            anim.SetBool("isAiming", true);
            aimVirtualCamera.gameObject.SetActive(true);
            thirdPersonController.SetSensitivity(aimSensitivity);
            thirdPersonController.SetRotateOnMove(false);    
            
            Vector3 worldAimTarget = mouseWorldPosition;
            worldAimTarget.y = transform.position.y;
            Vector3 aimDirection = (worldAimTarget - transform.position).normalized;         
            //Rotando el personaje para que mire al objetivo
            transform.forward = Vector3.Lerp(transform.forward, aimDirection, Time.deltaTime * 20f);            
        }
        else
        {
            crosshair.SetActive(false);
            chargeSphere.SetActive(false);
            chargePointLight.SetActive(false);
            anim.SetBool("isAiming", false);
            aimVirtualCamera.gameObject.SetActive(false);
            thirdPersonController.SetSensitivity(normalSensitivity);
            thirdPersonController.SetRotateOnMove(true);
        }
        
        if (starterAssetsInputs.shoot)
        {
            if (anim.GetBool("isAiming") && !anim.GetBool("isCasting") && anim.GetBool("Grounded") &&
                !anim.GetBool("isArmed2") && !anim.GetBool("IsLockedOn"))
            {
                anim.SetBool("isCasting", true);
                anim.Play("Casting");
                aimDir = (mouseWorldPosition - spawnBulletPosition.position).normalized;
                Invoke("InstantiateProyectile", 0.35f);
            }            
            starterAssetsInputs.shoot = false;
        }
        if (anim.GetBool("IsLockedOn") && starterAssetsInputs.aim && !anim.GetCurrentAnimatorStateInfo(0).IsName("CastLockedOn") && canShoot)
        {
            canShoot = false;
            anim.SetTrigger("CastingWhileLocked");
            aimDir =-(transform.position - enmyFndr.GetTarget().transform.position).normalized;
            Invoke("ResetShoot", 0.5f);
            Invoke("InstantiateProyectileLockedOn", 0.35f);
            //starterAssetsInputs.shoot = false;
        }
    }
    private void InstantiateProyectile()
    {
        Instantiate(pfBulletProjectile, spawnBulletPosition.position, Quaternion.LookRotation(aimDir, Vector3.up));
    }
    private void InstantiateProyectileLockedOn()
    {
        Instantiate(pfBulletProjectile, spawnBulletPositionLockedOn.position, Quaternion.LookRotation(aimDir, Vector3.up));
    }
    private void ResetShoot()
    {
        canShoot = true;
    }

}
